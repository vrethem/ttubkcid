# Ttubkcid Showdown 
![logo](pictures/logo.png)
Ttubkcid Showdown is a multiplayer strategic game made in c++  

## Requirements
* linux
* g++  version 5.4.1 or higher
* SFML build 2.4.2

## Installation

A. [Download the repository](https://bitbucket.org/vrethem/worms/get/58ab986b9e6c.zip) 
	or clone it `> git clone https://vrethem@bitbucket.org/vrethem/worms.git`.

B. Open: `<repo>/`

C. Build and execute:
```
> make 
> bin/game
```

## Supports
* Keyboard and mouse controll 
* Audio output

##Design 
![design](UML_klass_diagram/2017-05-23.png)
UML-diagram
## Maps 
![nicePic](maps/Cave.png)
Maps can be made in any image editor you like. The program translates   
blue crosses to spawn points and white color to be empty space (transparent). 

##Made by
![credits](pictures/credits.png)
