

#ifndef PLAYER_H
#define PLAYER_H
#include "entity.h"
#include "team.h"

class Player
{
public:
    Player(std::vector<Entity*>* objects, int const wcount, Map_class*, int playernbr);

	~Player() = default;

    void dance();
    void update();
    void new_round();
    bool is_round_over();
    bool is_game_over();
    void delete_dead();
    int getNumber();
private:
    Team team;
    int pnumber;
};
#endif
