
#ifndef COLOR_LIB_H
#define COLOR_LIB_H
#include <vector>
#include <string>
#include <SFML/Graphics.hpp>
class Color_lib
{
public:
    static sf::Color getColor();
    static void restart();
private:
    static std::vector<sf::Color> colors;
};
#endif
