#ifndef ENTITY_H
#define ENTITY_H

#include<SFML/Graphics/RectangleShape.hpp>
#include<SFML/System/Vector2.hpp>
#include<SFML/Graphics/Sprite.hpp>
#include"texture_lib.h"
#include "map_class.h"
#include<string>
#include <iostream>

// Hitbox konstaner
#define W_SIZE_X 16
#define W_SIZE_Y 36
#define WEAPON_SIZE_X 40
#define WEAPON_SIZE_Y 20

// Aim point constanter 
#define AIM_SIZE_X 40
#define AIM_SIZE_Y 40

#define FIRECONE_SIZE_X 64
#define FIRECONE_SIZE_Y 64

// Sprite konstanter 
#define SPRITE_SIZE_WORM_X 40
#define SPRITE_SIZE_WORM_Y 30



class Entity 
{
public:
    //Entity()=delete;
    virtual ~Entity() {}

    Entity( sf::Vector2f const & size,sf::Vector2f const & pos, std::string const & texture)
        :hitbox{size},sprite{Texture_lib::getTexture(texture)}
    {
        hitbox.setFillColor(sf::Color::Green);
        hitbox.setPosition(pos); // Spriten s�tts i respektives update() functioner 
    }
    virtual void draw(sf::RenderWindow* window) = 0;
    virtual bool collision(Map_class &){return false;}
    //Override denna function i Worm och Projectile classen 
    
    virtual void update(sf::View &,Map_class &) = 0;

    sf::FloatRect getGlobalBounds() const { return hitbox.getGlobalBounds(); }
    sf::Vector2f getPosition() { return hitbox.getPosition(); }
		virtual Entity* getAdress() { return this; }
		virtual void printPosition() {}
    float getRotation() { return hitbox.getRotation(); }
    sf::Vector2f getScale() { return hitbox.getScale(); }
protected:
    sf::RectangleShape hitbox;
    sf::Sprite sprite;
};

#endif
