#ifndef WEAPONSTASH_H
#define WEAPONSTASH_H
#include"weapon.h"
#include<vector>
#include<utility>
#include<iterator>
#include<memory>

class WeaponStash
{
public:
    WeaponStash();
    ~WeaponStash();
    // WeaponStash(Weapon* const & item);
    // WeaponStash(std::initializer_list<Weapon> list);
    void add(Weapon*  item);
    std::pair<Weapon,int>& at(int index);
    void addRandom(){}; // för att lägga till random vapen vid ev drop;
    void removeZero();
private:
   
    std::vector< std::pair<Weapon*,int> > stash;
    bool member(Weapon* const & item) const; 
    auto stashBegin() 
    {
        return stash.begin();
    }
    auto stashEnd() 
    {
        return stash.end();
    }


public:
    class CircularIterator
    {
    public: 
        typedef CircularIterator iterator;
        typedef std::pair<Weapon*,int> valuetype;
        typedef std::vector<std::pair<Weapon*, int> >::iterator pointer;
        typedef std::pair<Weapon*, int>& reference;
        typedef int differens_type;
        typedef std::bidirectional_iterator_tag iterator_category;

        CircularIterator(WeaponStash* const & obj , std::vector< std::pair<Weapon*,int> >::iterator const & ptr_)
            :obj_ptr{obj},iter_ptr{ptr_}
        {}  
        iterator & operator++();
        iterator operator++(int);
        iterator & operator--();
        iterator operator--(int);
        reference operator*()
        {
            return *iter_ptr;
        }
        reference operator*() const
        {
            return *iter_ptr;
        }
        pointer operator->()
        {
            return iter_ptr;
        }

        bool operator==(iterator const& rhs)
        {
            return iter_ptr == rhs.iter_ptr;
        }

        bool operator!=(iterator const & rhs)
        {
            return iter_ptr!= rhs.iter_ptr;
        }
        void decrese();
        
    private:
        WeaponStash* obj_ptr;
        pointer iter_ptr;
       
    };

public:
    WeaponStash::CircularIterator::iterator begin(); 
    WeaponStash::CircularIterator::iterator end(); 

};


#endif


