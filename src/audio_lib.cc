#include "audio_lib.h"
#include <exception>
#include <SFML/Audio.hpp>

sf::SoundBuffer& Audio_lib::getSound(std::string const & s)
{
    auto search = lib.find(s);
    if(search == lib.end())
		{
				sf::SoundBuffer temp;
				if(!temp.loadFromFile("audio/" + s))throw std::logic_error(s + "audio missing ");
				Audio_lib::lib[s] = temp;
		}
    return lib[s];
}
void Audio_lib::playSound(std::string const & s)
{
    auto search = lib.find(s);
    if(search == lib.end())
        {
                sf::SoundBuffer temp;
                if(!temp.loadFromFile("audio/" + s))throw std::logic_error(s + "audio missing ");
                Audio_lib::lib[s] = temp;
        }

    Audio_lib::slib[s] = sf::Sound(lib[s]);
    slib[s].play();
}
void Audio_lib::stopSound(std::string const & s)
{
    auto search = lib.find(s);
    if(search != lib.end())
    {
        slib[s].stop();
    }
}

std::map<std::string,sf::Sound> Audio_lib::slib{};
std::map<std::string,sf::SoundBuffer> Audio_lib::lib{};
