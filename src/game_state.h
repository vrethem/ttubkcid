
#ifndef GAME_STATE
#define GAME_STATE
#include <SFML/Graphics.hpp>
#include "menu.h"
#include "world.h"
#include "state.h"
#include "options.h"
#include "credits.h"
#include "ingame_menu.h"

class Game_State
{
public:
    Game_State(sf::RenderWindow & _window);
    Game_State(Game_State const &) = delete;
    Game_State& operator=(Game_State const &) = delete;
    void draw();
    void update();
private:

    State state;
    Menu menu;
    World world;
    Options options;
    Ingame_menu ingame_menu;
    Credits credits;
    sf::RenderWindow* window;
};
#endif
