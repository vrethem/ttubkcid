
#include "world.h"
#include <SFML/Graphics.hpp>
#include "texture_lib.h"
#include <vector>
#include <iostream>
#include <memory>
#include "font_lib.h"
#include "audio_lib.h"
#include "color_lib.h"
#include "name_lib.h"
World::World(sf::RenderWindow* _window) : 
    playerv{},objects{},map{},view{sf::FloatRect(0,0,300,300)},currentplayer{},
    gameover{false}, window{_window},time{"0",Font_lib::getFont("arial.ttf"),45},clock{}, water{},background{}, timer{}
 {
     playertext.setFont(Font_lib::getFont("arial.ttf"));
     playertext.setFillColor(sf::Color::Red);
     playertext.setString("Player 1");
     playertext.setCharacterSize(60);

     time.setPosition(window->getSize().x-100,window->getSize().y-115);
     time.setFillColor(sf::Color::Black);
     clock.setTexture(Texture_lib::getTexture("clock.png"));
     clock.setPosition(window->getSize().x-160,window->getSize().y-200);
     water.setTexture(Texture_lib::getTexture("water.png"));
     water.setPosition(0, window->getSize().y - 30);
     background.setTexture(Texture_lib::getTexture("background1.png"));
 }

void World::draw()
{
    window->draw(background);
    map.draw(window);
    if (roundtime >= 57 && !newRound)
    window->draw(playertext);

    // window->setView(view); 
    for (Entity* e : objects)
    {
        e->draw(window);
    }
    window->draw(water);
    window->draw(clock);
    window->draw(time);
}
void World::update()
{
		playertext.setPosition(window->getView().getCenter().x - playertext.getGlobalBounds().width/2,50);
    moveWater();
    if (!gameover)
    {
	if (timer.getElapsedTime().asSeconds() >= 1)
	{
	    roundtime--;
	    timer.restart();
	}
	time.setString(std::to_string(roundtime));

	if (roundtime <= 10 && time.getFillColor() != sf::Color::Red)
	    time.setFillColor(sf::Color::Red);

	if (!newRound)
	    (*currentplayer)->update();

	for (Entity* e : objects)
	{
	    e->update(view, map);
	}



	if ( (*currentplayer)->is_round_over() || roundtime <= 0)
	{
	    if (!newRound)
	    {
		newRound = true;
		roundtime = 3;
		timer.restart();
	    }

	    if (roundtime <= 0)
	    {
		for (auto i = playerv.begin(); i < playerv.end(); ++i)
		{
		    (*i)->delete_dead();
		    if ((*i)->is_game_over())
		    {
			i = playerv.erase(i);
		    
		    }
		}
	    

		if (playerv.size() == 1)
		{
		    gameover = true;
            Audio_lib::playSound("winning.wav");
            Audio_lib::playSound("Clap.wav");
		    playertext.setString("Victory/nPlayer "+std::to_string(playerv.front()->getNumber()));
		    newRound = false;
		    return;
		}
		else if (playerv.size() == 0)
		{
		    gameover = true;
		    playertext.setString("Tie");
		    newRound = false;
		    return;
		}
		newRound = false;
		roundtime = 60;
		timer.restart();
		time.setFillColor(sf::Color::Black);
        Audio_lib::playSound("StartRound.wav");
		next_player();
		playertext.setString("Player " + std::to_string((*currentplayer)->getNumber()));
		(*currentplayer)->new_round();
                map.lowerImage(4);
	    }
	}
    }
    

    if (gameover)
    {
	for (unsigned int i{0}; i < playerv.size(); ++i)
	{
	    playerv.at(i)->dance();
	}
	timer.restart();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape))
    {
	changeState("ingame_menu");
    }
}
void World::next_player()
{
    ++currentplayer;

    if (currentplayer == playerv.end())
    { 
     	currentplayer = playerv.begin();
    }
}
void World::start(int const _wcount,int const _pcount, std::string const & mapname)
{
    //Map_class temp{mapname};
    //map = std::move(temp);
		map.start(mapname);
    pcount = _pcount;
    wcount = _wcount;
    for (int i{}; i<pcount; i++)
    {
	playerv.push_back(std::make_unique<Player>(&objects,wcount, &map,i+1));
	time.setFillColor(sf::Color::Black);
    }
    currentplayer = playerv.begin();
    (*currentplayer)->new_round();
    Audio_lib::playSound("StartRound.wav");

    timer.restart();
}
 void World::clear()
 {
     roundtime = 60;
     counter = 0;
     newRound = false;
     playerv.clear();
     objects.clear();
     gameover = false;
     Name_lib::restart();
     Color_lib::restart();
     
 }

void World::moveWater()
{
    if((water.getPosition().x <=  window->getSize().x - water.getGlobalBounds().width))
        water.setPosition(sf::Vector2f(0, window->getSize().y - 30));
    else
        water.move(-0.25,0);
}

