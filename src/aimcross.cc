#include "aimcross.h"

Aim::Aim(sf::Vector2f const & pos):Entity{sf::Vector2f{AIM_SIZE_X,AIM_SIZE_Y},pos,"cross.png"}
{
    Entity::sprite.setScale(
        AIM_SIZE_X/(Entity::sprite.getLocalBounds()).width,
        AIM_SIZE_Y/(Entity::sprite.getLocalBounds()).width);
}

void Aim::setPosition(sf::Vector2f const & pos)
{
    Entity::hitbox.setPosition(pos);
    sf::FloatRect rect{Entity::hitbox.getLocalBounds()};
    Entity::hitbox.setOrigin(rect.left+(rect.width/2),rect.top+(rect.height/2));
}

void Aim::setOrigin(sf::Vector2f const & org)
{
    Entity::hitbox.setOrigin(org);
}

void Aim::draw(sf::RenderWindow*  window)
{
     window->draw(Entity::sprite);
     // window->draw(Entity::hitbox);    
}

void Aim::update(sf::View &, Map_class &)
{
    Entity::sprite.setOrigin(Entity::hitbox.getOrigin());
    Entity::sprite.setPosition(Entity::hitbox.getPosition());

}
