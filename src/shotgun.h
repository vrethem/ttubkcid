#ifndef SHOTGUN_H
#define SHOTGUN_H
#include "weapon.h"
#include<SFML/System/Vector2.hpp>


class Shotgun : public Weapon
{
public:
    Shotgun() = delete;
    Shotgun(std::vector<Entity*>* obj_ptr );

    
    void prepareFire() override;
    
    void fireUpdate() override;
private:
    float shotgun_damage{ 15 };
    float weight{ 2.5 };
    float def_speed{ 2.8 };
    sf::Vector2f projectile_size{8,9};

    
    void fire(float speed) override;
};

#endif
