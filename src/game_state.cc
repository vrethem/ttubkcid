#include "game_state.h"
#include <SFML/Graphics.hpp>

Game_State::Game_State(sf::RenderWindow & _window) : state{}, menu{&world,&options},world{&_window},options{},ingame_menu{&world}, credits{}, window{&_window}
{
    
}

void Game_State::draw()
{
    std::string stringstate = state.getState();

    if (stringstate == "menu")
    {
	menu.draw(window);
    }
    else if (stringstate == "world")
    {
	world.draw();
    }
    else if (stringstate == "options")
    {
	options.draw(window);
    }
    else if (stringstate == "ingame_menu")
    {
	world.draw();
	ingame_menu.draw(window);
    }
    else if (stringstate == "credits")
    {
	credits.draw(window);
    }

}
void Game_State::update()
{

    std::string stringstate = state.getState();

    if (stringstate == "menu")
    {
	menu.update(window);
    }
    else if (stringstate == "world")
    {
	world.update();
    }
    else if (stringstate == "options")
    {
	options.update(window);
    }
    else if (stringstate == "ingame_menu")
    {
	ingame_menu.update(window);
    }
    else if (stringstate == "credits")
    {
	credits.update();
    }
 
}
