#include "name_lib.h"
#include <random>
#include <iostream>

std::vector<std::string> Name_lib::names{"Bert","Greger","Bengt","Tobias","Benny","Bob","Per","Sickan","Harry","Chris","Albin","Anders","Jacob","Kurt","Banane","Lennart","Gert","Tony","Albert","Nils","Hans","Gunnar"};
    
std::string Name_lib::getName()
{
    int high = names.size()-1;
    int low = 0;
    std::random_device rnd;
    int random = rnd() % ( high - low + 1 ) + low;
    std::string name = names.at(random);
    names.erase(names.begin()+random);
    return name;
}
void Name_lib::restart()
{
    names = std::vector<std::string>{"Bert","Greger","Bengt","Tobias","Benny","Bob","Per","Sickan","Harry","Chris","Albin","Anders","Jacob","Kurt","Banane","Lennart","Gert","Tony","Albert","Nils","Hans","Gunnar"};
}
