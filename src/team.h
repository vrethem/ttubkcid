#ifndef TEAM_H
#define TEAM_H

#include "weapon.h"
#include "entity.h"
#include "worm.h"
#include <map>
#include <vector>
#include <iterator>
#include <memory>
#include "bazooka.h"
#include "weaponstash.h"

class Team
{
public:
	~Team() = default;
	Team(std::vector<Entity*>* _objects, int const wcount, Map_class*);

	Team(const Team&) = delete;
	Team& operator=(const Team&) = delete;

	void dance();
	void updateRound();
	void next_worm();
	bool get_active();
	bool is_game_over();
	void delete_dead();

private:
	std::vector<std::unique_ptr<Worm>>::iterator selected_worm;
	std::vector<std::unique_ptr<Worm>> worms;
	std::vector<Entity*>* objects;
	WeaponStash weapons;
};

#endif 
