#include "map_class.h"
#include <exception>
#include <iostream>
#include <math.h>
#include <random>
#include <algorithm>

#define PI 3.14159265359

// Konstruktorer 
 Map_class::Map_class()
     :spawn_points(),sp_iter{},image(),sprite(),text()
     {
     }
Map_class::Map_class(std::string map)
    :spawn_points(),sp_iter{}, image(), sprite(), text()
{
    if (!image.loadFromFile("maps/" + map)) throw std::logic_error(map + " missing");
    init();
    set_sprite();
}

void Map_class::start(std::string const & name)
{
    if (!image.loadFromFile("maps/" + name)) throw std::logic_error(name + " missing");
    init();
    set_sprite();
}
//Public
// Call insideImageFrame() before calling collision !!
bool Map_class::insideImageFrame(sf::FloatRect const & rect) const
{ 
    return (!(rect.left + rect.width > image.getSize().x || rect.left < 0 || rect.top + rect.height > image.getSize().y || rect.top < -150));
}
bool Map_class::insideImage(sf::FloatRect const & rect) const
{ 
    return (!(rect.left + rect.width > image.getSize().x || rect.left < 0 || rect.top + rect.height > image.getSize().y || rect.top < 0));
}

void Map_class::eraseCircle(sf::CircleShape const & c)
{
    // Loopa lla pixlar i boxen 
    for (int x{ (int)c.getGlobalBounds().left }; x < c.getGlobalBounds().left + c.getGlobalBounds().width; x++)
    {
        for (int y{ (int)c.getGlobalBounds().top }; y < c.getGlobalBounds().top + c.getGlobalBounds().height; y++)
        {
            // Avbryt om man frsker komma t odefinerade punkter i image
            if (x >= (int)image.getSize().x || x <= 0 || y >= (int)image.getSize().y || y <= 0)
                break;
            // Ta bort = stt till transparant pixel 
            if (sqrt(powf((x - c.getGlobalBounds().width / 2 - c.getPosition().x), 2) +
                     powf((y - c.getGlobalBounds().height / 2 - c.getPosition().y), 2)) <= c.getRadius()) 
                image.setPixel(x, y, sf::Color{ 55, 55, 55, 0 });
        }
    }
    set_sprite();
}

bool Map_class::collision(sf::FloatRect const & rect)
{
    if (!insideImage(rect)) return false;
    // Kolla varannan punkt lngs den vre och undre linjen 
    int count_hits{};
    for (int x{ (int)rect.left }; x < rect.left + rect.width; x += 2)
    {
        if (image.getPixel(x, int(rect.top)).a > 10)
            count_hits++;
        if (image.getPixel(x, int(rect.top + rect.height)).a > 10)
            count_hits++;
    }
    //Kolla varannan punkt lngfs den vnstra och hgra linjen 
    for (int y{ (int)rect.top }; y < rect.top + rect.height; y += 2)
    {
        if (image.getPixel(int(rect.left), y).a > 10)
            count_hits++;
        if (image.getPixel(int(rect.left + rect.width), y).a > 10)
            count_hits++;
    }
    // Kollision om fler n X trffar 
    return (count_hits > 2);
}

void Map_class::draw(sf::RenderWindow* window)
{
    return window->draw(sprite);
}


bool Map_class::collision(sf::CircleShape const & circle) const
{
    insideImageFrame(circle.getGlobalBounds());
    // Kolla varje punkt lngs den vre och undre linjen 
    int count_hits{};
    int x = int(circle.getPosition().x + circle.getRadius());
    int y = int(circle.getPosition().y + circle.getRadius());
    // Kolla 360 / 4 <=> 90 punkter lngs med cirkeln 
    for (int grad{ 0 }; grad <= 360; grad += 4)
    {
        if (image.getPixel(x + (int)circle.getRadius()*cos(grad*PI / 180), y + (int)circle.getRadius()*sin(grad*PI / 180)).a > 10)
            count_hits++;
    }
    // Kollision om fler n X trffar 
    return (count_hits >= 2);
}


// Getters
const sf::Image* Map_class::get_image() const
{
    return &image;
}

const std::vector<sf::Vector2f>* Map_class::get_spawn_points() const
{
	return &spawn_points;
}

sf::Vector2f Map_class::get_spawn_point()
{ 
    if (sp_iter == spawn_points.end())
    {
        sp_iter = spawn_points.begin();
    }
		std::cout<< "get_spawn_point X,Y: " << sp_iter->x << ", " << sp_iter->y  << std::endl; 
    return *(sp_iter++);
}

void Map_class::lowerImage(int y)
{
	sf::Image new_image;
	new_image.create(image.getSize().x, image.getSize().y, sf::Color(0, 0, 0, 0));
	new_image.copy(image, 0, y, sf::IntRect(0, 0, image.getSize().x, image.getSize().y - y), false);
	image = new_image;
	set_sprite();
}
// Private 
void Map_class::init()
{
    spawn_points.clear();
    unsigned int x{};
    const sf::Uint8* ptr = image.getPixelsPtr();
    const sf::Uint8* ptr_end = ptr + image.getSize().x * image.getSize().y * 4;
    while (ptr < ptr_end)
    {
        x++;  // [196][133][167]
         if (ptr[0] == 196 && ptr[1] == 133 && ptr[2] == 167)
         {
	     spawn_points.push_back(sf::Vector2f(x%image.getSize().x - SPAWN_CROSS/2, x/image.getSize().x - SPAWN_CROSS/2));
             std::cout << "New spawn point(x,y): ("<< x%image.getSize().x - SPAWN_CROSS/2 << ','
                       << x/image.getSize().x - SPAWN_CROSS/2<< ")\n";
	 }
	 
         ptr += 4;
    }
    // Stt rent bl, vita samt spawnfrg pixlar till transparenta 
    std::cout << "Totally " << spawn_points.size() << " spawn points. \n";
    image.createMaskFromColor(sf::Color{ 0, 0, 255 }, 0);
    image.createMaskFromColor(sf::Color{196, 133, 167 }, 0);
    image.createMaskFromColor(sf::Color{255,255,255},0 );

    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(spawn_points.begin(), spawn_points.end(), g);
    sp_iter = spawn_points.begin();
}

void Map_class::set_sprite()
{
    text.loadFromImage(image);
    sprite.setTexture(text);
}
Map_class& Map_class::operator=(Map_class const & _map)
{
    *this = _map;
    return *this;
}
Map_class& Map_class::operator=(Map_class const && _map)
{
    this->spawn_points = _map.spawn_points;
    this->sp_iter = _map.sp_iter;
    this->image = _map.image;
    this->sprite = _map.sprite;
    this->text = _map.text;
    this->init();
    this->set_sprite();
    
    return *this;
}
