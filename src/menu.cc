#include "font_lib.h"
#include "audio_lib.h"
#include "menu.h"
#include <iostream>
#include <random>

Menu::Menu(World* _world,Options* _options) : bakgroundtexture{},bakground{},start{},name2{},quit{},name{},options{},credits{},world{_world},optionsptr{_options},backgroundSound{},view{sf::FloatRect{0,0,800,800}},mousepos{}, shapes{}
{
    
    bakgroundtexture.loadFromFile("texture/menubakground.png");
    bakground.setTexture(bakgroundtexture);

    backgroundSound.openFromFile("audio/background.wav");
    backgroundSound.setVolume(60);
    backgroundSound.setLoop(true);
    backgroundSound.play();

    start.setFont(Font_lib::getFont("arial.ttf")); 
    quit.setFont(Font_lib::getFont("arial.ttf"));

    name.setFont(Font_lib::getFont("headline2.ttf"));
    name2.setFont(Font_lib::getFont("headline.ttf"));
    options.setFont(Font_lib::getFont("arial.ttf"));
    credits.setFont(Font_lib::getFont("arial.ttf"));

    start.setString("Start");
    quit.setString("Quit");
    name.setString("ttubkcid");
    name2.setString("Showdown");
    options.setString("Options");
    credits.setString("Credits");

    name.setPosition(230.0f,75.0f);
    start.setPosition(330.0f,300.0f);
    options.setPosition(330.0f,375.0f);
    credits.setPosition(330.0f,450.0f);
    quit.setPosition(330.0f,525.0f);
    name2.setPosition(200.0f,145.0f);

    name2.setFillColor(sf::Color::Black);
    name.setFillColor(sf::Color::Yellow);


    name.setCharacterSize(80);
    name2.setCharacterSize(100);
    start.setCharacterSize(60);
    quit.setCharacterSize(60);
    options.setCharacterSize(60);
    credits.setCharacterSize(60);

    options.setOutlineColor(sf::Color::Black);
    name.setOutlineColor(sf::Color::Black);
    name2.setOutlineColor(sf::Color::White);
    start.setOutlineColor(sf::Color::Black);
    quit.setOutlineColor(sf::Color::Black);
    options.setOutlineColor(sf::Color::Black);
    credits.setOutlineColor(sf::Color::Black);
/*
    name.setOutlineThickness(10.0f);
    name2.setOutlineThickness(1.0f);
    start.setOutlineThickness(1.0f);
    quit.setOutlineThickness(1.0f);
    options.setOutlineThickness(1.0f);
    credits.setOutlineThickness(1.0f);
*/
    options.setStyle(sf::Text::Bold);
    credits.setStyle(sf::Text::Bold);
    start.setStyle(sf::Text::Bold);
    quit.setStyle(sf::Text::Bold);
    options.setStyle(sf::Text::Bold);
    name.setStyle(sf::Text::Bold || sf::Text::Italic);
    name2.setStyle(sf::Text::Bold || sf::Text::Italic);
    
    add_all_shapes();
    
}
void Menu::draw(sf::RenderWindow* window)
{
    window->draw(bakground);

    for (std::pair<sf::RectangleShape,int> s : shapes)
    {
	window->draw(s.first);
    }
    window->setView(view);

    window->draw(start);
    window->draw(quit);
    window->draw(name);
    window->draw(name2);
    window->draw(options);
    window->draw(credits);

}

void Menu::update(sf::RenderWindow* window)
{
    mousepos = sf::Mouse::getPosition(*window);
    sf::Rect<int> startrect{window->mapCoordsToPixel(start.getPosition()).x,window->mapCoordsToPixel(start.getPosition()).y,300,80};
    sf::Rect<int> creditsrect{window->mapCoordsToPixel(credits.getPosition()).x,window->mapCoordsToPixel(credits.getPosition()).y,300,80};
    sf::Rect<int> optionsrect{window->mapCoordsToPixel(options.getPosition()).x,window->mapCoordsToPixel(options.getPosition()).y,300,80};
    sf::Rect<int> quitrect{window->mapCoordsToPixel(quit.getPosition()).x,window->mapCoordsToPixel(quit.getPosition()).y,300,80};
    
    if (startrect.contains(mousepos))
    {
	if (start.getFillColor() != sf::Color::Red)
	{
        Audio_lib::playSound("buttonhover.wav");
	    start.setFillColor(sf::Color::Red);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
	    backgroundSound.stop();
	    world->start(optionsptr->getworm(),optionsptr->getplayer(),optionsptr->getMap());
	    window->setView(window->getDefaultView());
	    changeState("world");
	}
    }
    else if (start.getFillColor() != sf::Color::White)
    {
	start.setFillColor(sf::Color::White);
    }

	if (creditsrect.contains(mousepos))
    {
	if (credits.getFillColor() != sf::Color::Red)
	{
        Audio_lib::playSound("buttonhover.wav");
	    credits.setFillColor(sf::Color::Red);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
	    changeState("credits");
	}
    }
    else if (credits.getFillColor() != sf::Color::White)
    {
	credits.setFillColor(sf::Color::White);
    }

	if (optionsrect.contains(mousepos))
    {
	if( options.getFillColor() != sf::Color::Red)
	{
	    
        Audio_lib::playSound("buttonhover.wav");
	    options.setFillColor(sf::Color::Red);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
	    changeState("options");
	}
    }
    else if (options.getFillColor() != sf::Color::White)
    {
	options.setFillColor(sf::Color::White);
    }

	if (quitrect.contains(mousepos))
    {
	if (quit.getFillColor() != sf::Color::Red)
	{
        Audio_lib::playSound("buttonhover.wav");
	    quit.setFillColor(sf::Color::Red);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
	    window->close();
	}
    }
    else if (quit.getFillColor() != sf::Color::White)
    {
	quit.setFillColor(sf::Color::White);
    }

    

    for (std::pair<sf::RectangleShape,int> &s : shapes)
    {
        s.first.move(0.0f,s.second);
	if (s.first.getPosition().y >= 800)
	    out_of_range(s);
    }
}
int rng(const int low,const int high)
{
    std::random_device rnd;
    return rnd() % (high - low + 1 ) + low;
}
void Menu::add_all_shapes()
{
    for (int i{}; i<=800; i++)
    {
	add_shape();
    }
}
void Menu::add_shape()
{
    sf::RectangleShape a{sf::Vector2f(1.0f,2.0f)};
    a.setPosition(rng(1,800),rng(-400,0));
    a.setFillColor(sf::Color::Blue);
    int random = rng(3,8);
    shapes.push_back(std::pair<sf::RectangleShape,int>(a,random));
}
void Menu::out_of_range(std::pair<sf::RectangleShape,int>& shape)
{
    shape.first.setPosition(rng(1,800),0);
    shape.second = rng(3,8);
}

