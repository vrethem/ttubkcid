#include "player.h"

Player::Player(std::vector<Entity*>* objects, int const wcount, Map_class* map, int playernbr)
    : team{objects,wcount, map}, pnumber{playernbr}
{
}
void Player::dance()
{
    team.dance();
}
void Player::update()
{
    team.updateRound();
}
void Player::new_round()
{
    team.next_worm();
}
bool Player::is_round_over()
{
    return !team.get_active();
}
bool Player::is_game_over()
{
    return team.is_game_over();
}
void Player::delete_dead()
{
    team.delete_dead();
}
int Player::getNumber()
{
    return pnumber;
}
