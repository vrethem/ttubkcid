#ifndef TEXTURE_LIB_H
#define TEXTURE_LIB_H

#include <SFML/Graphics.hpp>
#include <string>
#include <map>
class Texture_lib
{
public:
    static sf::Texture& getTexture(std::string s);
    //static void addTexture(std::string s);
private:
    static std::map<std::string,sf::Texture> lib;
};


#endif
//Load texture with Texture_lib::addTexture(string)
//Get texture with Texture_lib::getTexture(string)
