#ifndef PROJECTILE_H
#define PROJECTILE_H

#include <SFML/Graphics.hpp>
#include "entity.h"
#include <vector>
#define SPEED 1

class Projectile :public Entity
{
public:
    virtual ~Projectile() { std::cout << "Projectile destructor \n";}
    Projectile() = default;

    Projectile(const Projectile&) = default;
    Projectile& operator=(const Projectile&) = default;
    
    Projectile(sf::Vector2f size,sf::Vector2f pos,float rotation,
               float speed,std::string texture,
               std::vector<Entity*>* objects_ptr, float kg, float damage);
    
    void update(sf::View& view, Map_class &) override;
    void updateMovement();
    bool collision(Map_class &) override;
    void draw(sf::RenderWindow * window);
    bool ifHit() const { return hit; }

    Entity* getAdress() override { return this; }
    void printPosition() override { std::cout << "pos : " << hitbox.getPosition().x << ", " << hitbox.getPosition().y << std::endl; }

private:
    std::vector<Entity*>* objectsArrayPtr;

    void makeExplosion(sf::CircleShape const & circle);
    
    sf::Vector2f velocity;
    float maxdamage;
    float blast_radius;
    float weight;
    bool hit{ false };
    
    bool collisionWorm();
};
//En projektil som skapas med en storlek,position,rotation,hastighet och textur
//Skadan och explosion samt tyngd är baserad på denns storlek

#endif
