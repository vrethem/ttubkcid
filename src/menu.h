#ifndef MENU_H
#define MENU_H
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>
#include "state.h"
#include "world.h"
#include "options.h"
class Menu : State
{
public:
    Menu(World* _world,Options* _options);
    Menu(const Menu&) = delete;
    Menu operator=(const Menu&) = delete;
    void draw(sf::RenderWindow* window);
    void update(sf::RenderWindow* window);
private:
    sf::Texture bakgroundtexture;
    sf::Sprite bakground;
    
    sf::Text start;
    sf::Text name2;
    sf::Text quit;
    sf::Text name;
    sf::Text options;
    sf::Text credits;
    World* world;
    Options* optionsptr;
    sf::Music backgroundSound; //borde bytas ut till music variablen
    sf::View view;

    sf::Vector2i mousepos;
    std::vector< std::pair<sf::RectangleShape,int> > shapes;
    void add_all_shapes();
    void add_shape();
    void out_of_range(std::pair<sf::RectangleShape,int>& shape);
};
#endif
