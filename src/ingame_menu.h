
#ifndef INGAME_MENU_H
#define INGAME_MENU_H
#include "state.h"
#include <SFML/Graphics.hpp>
#include "world.h"
class Ingame_menu : public State
{
public:
    Ingame_menu(World* _world);

    Ingame_menu(const Ingame_menu&) = delete;
    Ingame_menu operator=(const Ingame_menu&) = delete;

    void draw(sf::RenderWindow* window);
    void update(sf::RenderWindow* window);
private:
    sf::RectangleShape board;
    sf::Text resume;
    sf::Text restart;
    sf::Text exit;

    sf::Sound boop;

    World* world;

    
    
};
#endif
