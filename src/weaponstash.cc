#include"weaponstash.h"
#include<algorithm>

WeaponStash::WeaponStash()
    :stash{}
{}
WeaponStash::~WeaponStash()
{
    for(auto it{stash.begin()};it != stash.end();it++)
        delete it->first;
}
/*
WeaponStash::WeaponStash(Weapon* const & item)
    :stash{}
{
    stash.push_back(std::pair<Weapon*,int> {std::make_unique<Weapon>(item),1});
}

WeaponStash::WeaponStash(std::initializer_list<Weapon> list)
    :stash{}
{
    for(Weapon item: list)
    {
        if(member(item))
        {
            for(auto wep: stash)
            {
                if( item.getName() == (wep.first->getName())
                {
                    wep.second+=1;
                    break;
                }
            }
        }
        else 
        {
            stash.push_back(std::pair<Weapon,int>{&item,1});
        }
    }   
}
*/
void WeaponStash::add(Weapon*  item)
{
    if(member(item))
    {
        for(auto it{stash.begin()}; it != stash.end(); it++)
        {
            if( item->getName() == ( it->first->getName() ) )
            {
                it->second+=1;
                delete item;
                break;
            }
        }
    }
    else
    {
        stash.push_back( std::pair<Weapon*,int>{ item, 1 } );
    }
    std::cout<< "added " << item->getName() << std::endl;
}

  void WeaponStash::removeZero()
  {    
  auto ind = remove_if(stash.begin(),stash.end(),
                         [](std::pair<Weapon*,int>  & item)
                         {
                             if(item.second== 0)
                             {
                                 delete item.first;
                                 return true;
                             }
                             return false;
                         });
    stash.erase(ind,stash.end()); 
}

bool WeaponStash::member(Weapon* const & weap) const
{
    for(auto it{stash.begin()}; it!=stash.end();it++)
    {
        if(weap->getName() == it->first->getName() )
            {
                return true;
            }
            }
        return false;
    }

WeaponStash::CircularIterator::iterator& 
WeaponStash::CircularIterator::operator++()
{
    ++iter_ptr;
    if(iter_ptr==obj_ptr->stashEnd())
    {
        iter_ptr=obj_ptr->stashBegin();
    }
    return *this;
}

WeaponStash::CircularIterator::iterator 
WeaponStash::CircularIterator::operator++(int)
{

    auto temp =*this;
    ++(*this);
    return temp;
    
}

 

WeaponStash::CircularIterator::iterator& 
WeaponStash::CircularIterator::operator--()
{

    if(iter_ptr==obj_ptr->stashBegin())
    {
        iter_ptr=obj_ptr->stashEnd();
    }
    --iter_ptr;    
    return *this;

}


WeaponStash::CircularIterator::iterator
 WeaponStash::CircularIterator::operator--(int)
{
    auto temp =*this;
    --(*this);
    return temp;
    
}

void  WeaponStash::CircularIterator::decrese()
{
    
}

WeaponStash::CircularIterator::iterator 
WeaponStash::begin()
{
    return CircularIterator{this,stashBegin()};
}

WeaponStash::CircularIterator::iterator 
WeaponStash::end()
{
    return CircularIterator{this,stashEnd()};
}



