#ifndef FIRECONE_H
#define FIRECONE_H
#include"entity.h"

class FireCone : public Entity
{
public:
    FireCone(sf::Vector2f const & pos);
    void setVisible();
    void setInvisible();
    void setOrigin(sf::Vector2f const & org);
    void setPosition(sf::Vector2f const & pos);
    sf::Vector2f getPosition();
    void setRotation(int rot);
    void update(sf::View &, Map_class &) override;
    void updateTexture(int ms);
    void draw(sf::RenderWindow* window) override;
private:
    bool visible=false;
    void animate();
};

#endif


