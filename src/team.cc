#include "team.h"
#include "bazooka.h"
#include "shotgun.h"
#include <iostream>
#include "color_lib.h"

Team::Team(std::vector<Entity*>* _objects,int const wcount, Map_class* map) : selected_worm{},worms{},objects{_objects},weapons{}
{
    std::cout<< "created team" << std:: endl;
       // add 10 bazooka's //
    for(int i{0}; i!=10;++i)
    {
        weapons.add(new Bazooka{_objects});
        weapons.add(new Shotgun{_objects});
    }
    
    sf::Color temp{Color_lib::getColor()};
    for (int i{}; i < wcount; i++)
    {
        worms.push_back(std::make_unique<Worm>(weapons.begin(), map->get_spawn_point(), temp));
        std::cout << "added Worm" << std::endl;
    }
    for (unsigned int i = 0;i < worms.size();i++)
    {
        objects->push_back(worms.at(i).get());
    }

    selected_worm = worms.begin();
    (*selected_worm)->activateWeapon();
}

void Team::dance()
{
    for(unsigned int i{0}; i < worms.size(); i++)
    {
	worms.at(i)->dance();
    }
}

void Team::updateRound()
{
    if (!worms.empty())
        (*selected_worm)->updateMovement();
}

void Team::next_worm()
{
    for(unsigned int i{0};i<worms.size();i++)
    {
        worms.at(i)->setVelocity(sf::Vector2f(0,0.1));
    }
    // (*selected_worm)->inactivateWeapon();
    if (selected_worm == worms.end())
        selected_worm = worms.begin();

    ++selected_worm;

    if (selected_worm >= worms.end())
        selected_worm = worms.begin();

    (*selected_worm)->setActive();
    (*selected_worm)->activateWeapon();
}

bool Team::get_active()
{
    if (!worms.empty())
        return (*selected_worm)->getActive();
    return false;
}

bool Team::is_game_over()
{
    std::cout << worms.size() << std::endl;
	return worms.size() == 0;
}

void Team::delete_dead()
{
	for (unsigned int i{}; i < worms.size(); i++)
	{
		worms.at(i)->setHealth(worms.at(i)->getHealth() - worms.at(i)->getDamage());
	}
    for (unsigned int i{}; i < worms.size(); i++)
    {
        if (worms.at(i)->getHealth() <= 0)
        {
            int counter{};
            for (Entity* & e : *objects)
            {
                if (worms.at(i)->getAdress() == e->getAdress())
                {
                    std::cout << "Erasing worm-pointer in objects at  ";
		    std::cout << "Erasing vector worm at pos: " << counter << std::endl;
                    e->printPosition();
                    objects->erase(objects->begin() + counter);
                    break; // Skip counter++ - Viktigt
                }
                counter++;
            }
            std::cout << "Erasing worm from vector in Team at ";
            worms.at(i)->printPosition();
            worms.erase(worms.begin() + i);
            ++i;
        }
    }
	
}
