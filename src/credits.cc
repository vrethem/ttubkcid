#include "credits.h"
#include "font_lib.h"

Credits::Credits() : texture{},background{},andersworm{"Anders",sf::Color::Red},albinworm{"Albin",sf::Color::Blue},chrisworm{"Christoffer",sf::Color::Green},tobbeworm{"Tobias",sf::Color::Magenta},currentsequence{},currentmovement{},scale{},endtext{}
{
    texture.loadFromFile("texture/creditbackground.png");
    background.setTexture(texture);

    endtext.setFont(Font_lib::getFont("headline.ttf"));
    endtext.setString("ttubkcid");
    endtext.setPosition(300.0f,400.0f);

    endtext.setCharacterSize(scale);
    endtext.setFillColor(sf::Color::Black);

    endtext.setOutlineColor(sf::Color::Red);
    //endtext.setOutlineThickness(3.0f);
}
void Credits::draw(sf::RenderWindow* window)
{
    window->draw(background);
    andersworm.draw(window);
    albinworm.draw(window);
    chrisworm.draw(window);
    tobbeworm.draw(window);
    window->draw(endtext);

}
void Credits::update()
{
    switch (currentsequence)
    {
    case 0:
	firstsequence();
	break;
    case 1:
	secondsequence();
	break;
    case 2:
	thirdsequence();
	break;
    case 3:
	fourthsequence();
	break;
    case 4:
	finalsequence();
	break;
    }

}
void Credits::firstsequence()
{
    andersworm.move();
    if (currentmovement == 0)
    {
	andersworm.left();
	if (andersworm.getPos().x <= 100)
	    currentmovement++;
    }
    else if (currentmovement == 1)
    {
	andersworm.right();
	if (andersworm.getPos().x >= 400)
	{
	    andersworm.jump();
	    currentmovement++;
	}
    }
    else if (currentmovement == 2)
    {

	andersworm.left();
	if (andersworm.getPos().x <= -100)
	{
	    currentsequence++;
	    andersworm.reset();
	    currentmovement = 0;
	}
    }
}
void Credits::secondsequence()
{
    albinworm.move();
    albinworm.left();
    albinworm.jump();

    if (albinworm.getPos().x <= -100)
    {
	currentsequence++;
	albinworm.reset();
	chrisworm.setPos(sf::Vector2f{0.0f,0.0f});
    }
    
}
void Credits::thirdsequence()
{
    chrisworm.move();
    chrisworm.right();
    if (chrisworm.getPos().y >= 600)
	chrisworm.jump();
    if (chrisworm.getPos().x >= 900)
    {
	currentsequence++;
	chrisworm.reset();
	
    }
}
void Credits::fourthsequence()
{
    tobbeworm.move();
    if (tobbeworm.getPos().x >= 400)
	tobbeworm.left();
    else if (currentmovement == 0)
    {
	tobbeworm.rare();
	currentmovement++;
    }
    else if (currentmovement == 1)
    {
	tobbeworm.jump();
	tobbeworm.left();
	if (tobbeworm.getPos().x <= -100)
	{
	    andersworm.setPos(sf::Vector2f{-100.0f,600.0f});
	    albinworm.setPos(sf::Vector2f{-250.0f,600.0f});
	    chrisworm.setPos(sf::Vector2f{900.0f,600.0f});
	    tobbeworm.setPos(sf::Vector2f{900.0f,0.0f});
	    currentsequence++;
	}
    }
}
void Credits::finalsequence()
{
    andersworm.move();
    albinworm.move();
    chrisworm.move();
    tobbeworm.move();
    if (andersworm.getPos().x <= 50)
	andersworm.right();
    else
	andersworm.rare();

    if (albinworm.getPos().x <= 250)
	albinworm.right();
    else
	albinworm.rare();

    if (chrisworm.getPos().x >= 575)
	chrisworm.left();
    else
        chrisworm.rare();

    if (tobbeworm.getPos().x >= 400)
    {
	tobbeworm.left();
	return;
    }
    else
	tobbeworm.rare();
    endtext.setCharacterSize(scale);
    endtext.move(-0.6,-0.6);
    scale += 0.5f;;
    
    if (scale >= 200)
    {
	scale = 0;
	endtext.setCharacterSize(scale);
	endtext.setPosition(300.0f,400.0f);
	currentmovement = 0;
	currentsequence = 0;
	andersworm.reset();
	albinworm.reset();
	chrisworm.reset();
	tobbeworm.reset();
	changeState("menu");
    }
}
