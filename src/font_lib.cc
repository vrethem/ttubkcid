#include "font_lib.h"
#include <exception>

sf::Font& Font_lib::getFont(std::string s)
{
    auto search = lib.find(s);
    if(search == lib.end())
    {
        sf::Font font;
        if(!font.loadFromFile("font/" + s))throw std::logic_error(s + "font missing ");
        Font_lib::lib[s] = font;
    }
    return lib[s];
}
std::map<std::string,sf::Font> Font_lib::lib{};
