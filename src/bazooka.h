#ifndef BAZOOKA_H
#define BAZOOKA_H
#include"weapon.h"
#include "audio_lib.h"

class Bazooka: public Weapon
{
public:
    Bazooka()=delete;
    Bazooka(std::vector<Entity*>* obj_ptr );
    ~Bazooka()=default ;
    void prepareFire() override;
    void fireUpdate() override;
private:
    void fire(float speed) override;
    float weight{ 10 };
    sf::Vector2f projectile_size{12,12};
};

#endif
