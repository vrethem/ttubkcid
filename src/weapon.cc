#include "weapon.h"
#include <stdexcept>
#include <math.h>
#include <memory>
#include <vector>
#include <iostream>
#include "audio_lib.h"
#define PI 3.14159265359
#define AIM_OFFSET 50

Weapon::Weapon(std::string nam,float dam, 
               std::vector<Entity*>* const & objPtr, sf::Vector2f const & size, 
               sf::Vector2f const & pos)
    :Entity{size,pos,nam+".png"},name{nam},damage{dam},aim_point{pos},
    objectsPtr{objPtr},projectiles{}, cone{pos}
{
    Entity::sprite.setScale(
        size.x/(Entity::sprite.getLocalBounds()).width,
        size.y/(Entity::sprite.getLocalBounds()).width);
    Entity::hitbox.setRotation(180);
    sprite.setScale(abs(sprite.getScale().x)*-1, sprite.getScale().y*-1);
}

int Weapon::getRotation()
{
    return Entity::hitbox.getRotation();
}
sf::Vector2f Weapon::getPosition()
{
    return Entity::hitbox.getPosition();
}
void Weapon::incElevation()
{
    if (left)
    {
        if(Entity::hitbox.getRotation()<250.f)
        {
            Entity::hitbox.rotate(0.4);
        }
    }
    else
    {
        if((Entity::hitbox.getRotation()>290.f)|(Entity::hitbox.getRotation()<71))
        {
            Entity::hitbox.rotate(-0.4);
        }
    }
    std::cout << "Rotation :  " << sprite.getRotation() << std::endl;
}

void Weapon::decElevation()
{
    if (left)
    {
        if(Entity::hitbox.getRotation()>110.f)
            Entity::hitbox.rotate(-0.4);
    }
    else
    {
        if(( Entity::hitbox.getRotation()<70.f ) | ( Entity::hitbox.getRotation()>280.f ) )
        {
            Entity::hitbox.rotate(0.4);
        }
    }
    std::cout << "Rotation :  " << sprite.getRotation() << std::endl;
}

void Weapon::setPosition(sf::Vector2f const & pos)
{
    sf::FloatRect rect{Entity::hitbox.getLocalBounds()};
    Entity::hitbox.setPosition(sf::Vector2f(pos.x,pos.y-rect.height/2));
    Entity::hitbox.setOrigin(rect.left+(rect.width/2),rect.top+(rect.height/2));
}

void Weapon::moveLeft()
{
    if (!left)
    {
        Entity::hitbox.setRotation(180 - Entity::hitbox.getRotation());
        sprite.setScale(abs(sprite.getScale().x)*-1, sprite.getScale().y*-1);
        sf::FloatRect rect{Entity::hitbox.getLocalBounds()};
        Entity::hitbox.setOrigin(rect.left-(rect.width/2),rect.top-(rect.height/2));
    }
    left = true;
}

void Weapon::moveRight()
{
    if (left)
    {
        Entity::hitbox.setRotation(180 - Entity::hitbox.getRotation());
        sprite.setScale(abs(sprite.getScale().x)*-1, sprite.getScale().y*-1);
        sf::FloatRect rect{Entity::hitbox.getLocalBounds()};
        Entity::hitbox.setOrigin(rect.left+(rect.width/2),rect.top+(rect.height/2));
    }
    left = false;
}

float radconv(float const & f)
{
    return f*(PI/180.0f);
}

void Weapon::update(sf::View & view, Map_class & map)
{
    for (Projectile& p : projectiles)
    {
        p.update(view, map);
    }
    // Ta bort projectiler vars ifHit()
    auto idx=std::remove_if(begin(projectiles),end(projectiles),[](Projectile & p){return p.ifHit(); });
    projectiles.erase(idx,end(projectiles));
    
    // updatera pos f�r aim_point//
    float positX{ (Entity::hitbox.getPosition().x ) + AIM_OFFSET* (cosf(radconv(Entity::hitbox.getRotation() ) ) ) };
    float positY{ (Entity::hitbox.getPosition().y ) + AIM_OFFSET* (sinf(radconv(Entity::hitbox.getRotation() ) ) ) };
    aim_point.setPosition(sf::Vector2f{positX,positY});
    aim_point.update(view , map);
    // f�r vapen 
    Entity::sprite.setPosition(Entity::hitbox.getPosition());
    Entity::sprite.setRotation(Entity::hitbox.getRotation());
    Entity::sprite.setOrigin(Entity::hitbox.getOrigin());

    cone.update(view,map);
}


void Weapon::draw(sf::RenderWindow* window)
{
    if (visible)
    {
        window->draw(Entity::sprite);
        // window->draw(Entity::hitbox);
        aim_point.draw(window);
    }
    // Rita projectiler
    for(Projectile & p :projectiles)
    {
        p.draw(window);
    }
    // Rita cone
    cone.draw(window);
}

void Weapon::resetWeapon()
{
    worm_state = beforeFire;
}
void Weapon::skipFire()
{
    worm_state = duringFire;
}
wormState Weapon::getState()
{
    return worm_state;
}


