
#ifndef NAME_LIB_H
#define NAME_LIB_H
#include <vector>
#include <string>
class Name_lib
{
public:
    static std::string getName();
    static void restart();
private:
    static std::vector<std::string> names;
};
#endif
