#include "texture_lib.h"
#include <exception>

sf::Texture& Texture_lib::getTexture(std::string s)
{
    auto search = lib.find(s);
    if(search == lib.end())
    {
        sf::Texture text;
        if(!text.loadFromFile("texture/" + s))throw std::logic_error(s + "texture missing ");
        Texture_lib::lib[s] = text;
    }
    return lib[s];
}

std::map<std::string,sf::Texture> Texture_lib::lib{};
