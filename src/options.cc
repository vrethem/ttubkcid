#include "font_lib.h"
#include "options.h"
#include <random>
#include <iostream>
#include "audio_lib.h"

int rngs(const int low,const int high)
{
    std::random_device rnd;
    return rnd() % (high - low + 1 ) + low;
}

Options::Options()
    : texturebackground{},maptexture{}, background{},texturearrow{},texturepressedarrow{},wormtexture{},backarrow{},addwormarrow{},subwormarrow{},addplayerarrow{},subplayerarrow{},
        wormsprite{},nextmap{},prevmap{},mapsprite{},headline{},worms{},player{},playertext{},wormtext{},maptext{},wormrotate{},
            wormcount{4},playercount{2},xmove{2},ymove{2},clock{}, arrows{},boop(7),maps{"Happy Face","A bad toilet day","Anders House","Cave"},currentmap{maps.begin()}
{   
    //hover.setBuffer(Audio_lib::getSound("buttonhover.wav"));

    texturebackground.loadFromFile("texture/optionsbackground.png");
    texturearrow.loadFromFile("texture/arrow.png");
    texturepressedarrow.loadFromFile("texture/arrowpressed.png");
    wormtexture.loadFromFile("texture/test.png");
    maptexture.loadFromFile("maps/Happy Face.png");

    mapsprite.setTexture(maptexture);
    background.setTexture(texturebackground);
    backarrow.setTexture(texturearrow);
    addwormarrow.setTexture(texturearrow);
    subwormarrow.setTexture(texturearrow);
    addplayerarrow.setTexture(texturearrow);
    subplayerarrow.setTexture(texturearrow);
    wormsprite.setTexture(wormtexture);
    nextmap.setTexture(texturearrow);
    prevmap.setTexture(texturearrow);

    mapsprite.setScale(0.2,0.2);
    mapsprite.setPosition(225,500);
    

    wormsprite.setTextureRect(sf::IntRect(0,0,32.0f,40.0f));
    wormsprite.setScale(5.0f,5.0f);
    wormsprite.setPosition(float(rngs(200,400)),float(rngs(200,400)));

    addwormarrow.setRotation(180.0f);
    addplayerarrow.setRotation(180.0f);
    nextmap.setRotation(180.0f);

    backarrow.setPosition(40.0f,40.0f);
    addwormarrow.setPosition(650.0f,400.0f);
    subwormarrow.setPosition(100.0f,350.0f);
    addplayerarrow.setPosition(650.0f,250.0f);
    subplayerarrow.setPosition(100.0f,200.0f);
    nextmap.setPosition(650.0f,550.0f);
    prevmap.setPosition(100.0f,500.0f);

    headline.setFont(Font_lib::getFont("headline.ttf"));
    worms.setFont(Font_lib::getFont("arial.ttf"));
    player.setFont(Font_lib::getFont("arial.ttf"));
    playertext.setFont(Font_lib::getFont("arial.ttf"));
    wormtext.setFont(Font_lib::getFont("arial.ttf"));
    maptext.setFont(Font_lib::getFont("arial.ttf"));

    headline.setPosition(230.0f,30.0f);
    worms.setPosition(350.0f,200.0f);
    player.setPosition(350.0f,350.0f);
    playertext.setPosition(300.0f,150.0f);
    wormtext.setPosition(300.0f,300.0f);
    maptext.setPosition(300.0f,450.0f);

    headline.setString("Options");
    worms.setString(std::to_string(playercount));
    player.setString(std::to_string(wormcount));
    playertext.setString("Players");
    wormtext.setString("Worms");
    maptext.setString("Happy Face");

    headline.setCharacterSize(80);
    worms.setCharacterSize(40);
    player.setCharacterSize(40);
    playertext.setCharacterSize(40);
    wormtext.setCharacterSize(40);
    maptext.setCharacterSize(40);

    headline.setOutlineColor(sf::Color::Black);
    worms.setOutlineColor(sf::Color::Black);
    player.setOutlineColor(sf::Color::Black);
    playertext.setOutlineColor(sf::Color::Black);
    wormtext.setOutlineColor(sf::Color::Black);
/*
    headline.setOutlineThickness(1.0f);
    worms.setOutlineThickness(1.0f);
    player.setOutlineThickness(1.0f);
    playertext.setOutlineThickness(1.0f);
    wormtext.setOutlineThickness(1.0f);
*/
    arrows.push_back(backarrow);
    arrows.push_back(addwormarrow);
    arrows.push_back(subwormarrow);
    arrows.push_back(addplayerarrow);
    arrows.push_back(subplayerarrow);

    for (sf::Sprite& arrow : arrows)
    {
	arrow.setColor(sf::Color(63,72,204));
    }
    nextmap.setColor(sf::Color(34,177,76));
    prevmap.setColor(sf::Color(34,177,76));

    arrows.push_back(nextmap);
    arrows.push_back(prevmap);
}
int Options::getplayer()
{
    return playercount;
}
int Options::getworm()
{
    return wormcount;
}
std::string Options::getMap()
{
    return maptext.getString() + ".png";
}
void Options::setnext()
{
    ++currentmap;

    if (currentmap == maps.end())
	currentmap = maps.begin();

    maptext.setString(*currentmap);
    setmap();
}
void Options::setprev()
{
    --currentmap;

    if (currentmap < maps.begin())
	currentmap = maps.end() - 1;
    
    maptext.setString(*currentmap);
    setmap();
}
void Options::setmap()
{
    maptexture.loadFromFile("maps/" + *currentmap + ".png");
    mapsprite.setTexture(maptexture);
    mapsprite.setScale(0.2,0.2);
    
}
void Options::draw(sf::RenderWindow* window)
{
    window->draw(background);
		window->draw(wormsprite);
    for (sf::Sprite& arrow : arrows)
    {
	window->draw(arrow);
    }
    window->draw(headline);
    window->draw(worms);
    window->draw(player);
    window->draw(playertext);
    window->draw(wormtext);
    window->draw(maptext);
    window->draw(mapsprite);
}
void Options::update(sf::RenderWindow* window)
{
    sf::Vector2i mousepos = sf::Mouse::getPosition(*window);
    int time = clock.getElapsedTime().asMilliseconds();

    sf::Rect<int> backrect{window->mapCoordsToPixel(backarrow.getPosition()).x,window->mapCoordsToPixel(backarrow.getPosition()).y,300,80};
    sf::Rect<int> addwormrect{window->mapCoordsToPixel(addwormarrow.getPosition()).x,window->mapCoordsToPixel(addwormarrow.getPosition()).y,-300,-80};
    sf::Rect<int> subwormrect{window->mapCoordsToPixel(subwormarrow.getPosition()).x,window->mapCoordsToPixel(subwormarrow.getPosition()).y,300,80};
    sf::Rect<int> addplayerrect{window->mapCoordsToPixel(addplayerarrow.getPosition()).x,window->mapCoordsToPixel(addplayerarrow.getPosition()).y,-300,-80};
    sf::Rect<int> subplayerrect{window->mapCoordsToPixel(subplayerarrow.getPosition()).x,window->mapCoordsToPixel(subplayerarrow.getPosition()).y,300,80};
    sf::Rect<int> nextmaprect{window->mapCoordsToPixel(nextmap.getPosition()).x,window->mapCoordsToPixel(nextmap.getPosition()).y,-300,-80};
    sf::Rect<int> prevmaprect{window->mapCoordsToPixel(prevmap.getPosition()).x,window->mapCoordsToPixel(prevmap.getPosition()).y,300,80};



    if (backrect.contains(mousepos))
    {
	if (!boop.at(0))
	{
	    boop.at(0) = true;
	    arrows.at(0).setTexture(texturepressedarrow);
        Audio_lib::playSound("buttonhover.wav");
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && time >=500)
	{
	    changeState("menu");
	    clock.restart();
	}
    }
    else
    {
	boop.at(0) = false;
	arrows.at(0).setTexture(texturearrow);
    }

    if (addwormrect.contains(mousepos) )
    {
	if (!boop.at(1))
	{
        Audio_lib::playSound("buttonhover.wav");
	    boop.at(1) = true;
	    arrows.at(1).setTexture(texturepressedarrow);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && wormcount < 4 && time >=500)
	{
	    ++wormcount;
	    clock.restart();
	}
    }
    else
    {
	boop.at(1) = false;
	arrows.at(1).setTexture(texturearrow);
    }

    if (subwormrect.contains(mousepos) )
    {
	if (!boop.at(2))
	    {
        Audio_lib::playSound("buttonhover.wav");
		boop.at(2) = true;
		arrows.at(2).setTexture(texturepressedarrow);
	    }
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && wormcount > 1 && time >=500)
	{
	    --wormcount;
	    clock.restart();
	}
    }
    else
    {
	boop.at(2) = false;
	arrows.at(2).setTexture(texturearrow);
    }
    if (addplayerrect.contains(mousepos))
    {
	if (!boop.at(3))
	{
        Audio_lib::playSound("buttonhover.wav");
	    boop.at(3) = true;
	    arrows.at(3).setTexture(texturepressedarrow);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && playercount < 4 && time >=500)
	{
	    ++playercount;
	    clock.restart();
	}
    }
    else
    {
	boop.at(3) = false;
	arrows.at(3).setTexture(texturearrow);
    }
    if (subplayerrect.contains(mousepos))
    {
	if (!boop.at(4))
	    {
        Audio_lib::playSound("buttonhover.wav");
		boop.at(4) = true;
		arrows.at(4).setTexture(texturepressedarrow);
	    }
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && playercount > 2 && time >=500)
	{
	    --playercount;
	    clock.restart();
	}
    }
    else
    {
	boop.at(4) = false;
	arrows.at(4).setTexture(texturearrow);
    }   

    if (nextmaprect.contains(mousepos))
    {
	if (!boop.at(5))
	    {
        Audio_lib::playSound("buttonhover.wav");
		boop.at(5) = true;
		arrows.at(5).setTexture(texturepressedarrow);
	    }
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && time >=500)
	{
	    setnext();
	    clock.restart();
	}
    }
    else
    {
	boop.at(5) = false;
	arrows.at(5).setTexture(texturearrow);
    }   


    if (prevmaprect.contains(mousepos))
    {
	if (!boop.at(6))
	    {
        Audio_lib::playSound("buttonhover.wav");
		boop.at(6) = true;
		arrows.at(6).setTexture(texturepressedarrow);
	    }
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && time >=500)
	{
	    setprev();
	    clock.restart();
	}
    }
    else
    {
	boop.at(6) = false;
	arrows.at(6).setTexture(texturearrow);
    }

    
    ++wormrotate;
    wormsprite.setRotation(wormrotate);
    wormsprite.move(xmove,ymove);
    bounce(wormsprite);


    worms.setString(std::to_string(playercount));
    player.setString(std::to_string(wormcount));
}
void Options::bounce(sf::Sprite & s)
{
    sf::Vector2f pos = s.getPosition();
    if (pos.x >= 800)
	xmove = -2.0f;
    else if (pos.x <= 0)
	xmove = 2.0f;
    else if (pos.y >= 800)
	ymove = -2.0f;
    else if (pos.y <= 0)
	ymove = 2.0f;
	     
}
