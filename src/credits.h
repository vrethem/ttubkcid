
#ifndef CREDITS_H
#define CREDITS_H
#include "state.h"
#include "creditsworm.h"
#include <SFML/Graphics.hpp>
class Credits : public State
{
public:
    Credits();
    void draw(sf::RenderWindow* window);
    void update();
    void firstsequence();
    void secondsequence();
    void thirdsequence();
    void fourthsequence();
    void finalsequence();
private:
    sf::Texture texture;
    sf::Sprite background;
    Creditsworm andersworm;
    Creditsworm albinworm;
    Creditsworm chrisworm;
    Creditsworm tobbeworm;
    int currentsequence;
    int currentmovement;
    float scale;
    sf::Text endtext;
    
};
#endif
