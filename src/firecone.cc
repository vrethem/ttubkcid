#include "firecone.h"
#include<iostream>
#include <math.h>
#define CONE_OFFSET 15
#define PI 3.14159265359

FireCone::FireCone(sf::Vector2f const & pos)
    :Entity{sf::Vector2f{FIRECONE_SIZE_X,FIRECONE_SIZE_Y},pos ,"shooting_cone.png"}
    {
        Entity::sprite.setScale(
            AIM_SIZE_X/(Entity::sprite.getLocalBounds()).width,
            AIM_SIZE_Y/(Entity::sprite.getLocalBounds()).width);
        Entity::sprite.setTextureRect(sf::IntRect(0,0,0,64));
    }
    
void FireCone::setVisible()
{
    visible=true;
}
void FireCone::setInvisible()
{
    visible=false;
}
void FireCone::setOrigin(sf::Vector2f const & org)
{
    Entity::hitbox.setOrigin(org);
}

float radconv2(float const & f)
{
    return f*(PI/180.0f);
}
void FireCone::setPosition(sf::Vector2f const & pos)
{
    float positX{ (pos.x ) + CONE_OFFSET * (cosf(radconv2(Entity::hitbox.getRotation() ) ) ) };
    float positY{ (pos.y ) + CONE_OFFSET * (sinf(radconv2(Entity::hitbox.getRotation() ) ) ) };
    Entity::hitbox.setPosition(sf::Vector2f{positX,positY});
      //  Entity::hitbox.setPosition(pos);
    sf::FloatRect rect{Entity::hitbox.getLocalBounds()};
    Entity::hitbox.setOrigin(rect.left,rect.top+(rect.height/2));
}

sf::Vector2f FireCone::getPosition()
{
    return Entity::hitbox.getPosition();
}
void FireCone::setRotation(int rot)
{
    Entity::hitbox.setRotation( rot );
}
void FireCone::update(sf::View &, Map_class &)
{
    Entity::sprite.setPosition(Entity::hitbox.getPosition() );
    Entity::sprite.setOrigin( Entity::hitbox.getOrigin() );
    Entity::sprite.setRotation( Entity::hitbox.getRotation() );
}
void FireCone::updateTexture(int  ms)
{
    ms++;
    int width{int(0.032*ms)};
    if (width <  11)
        width = 11;
    //  std::cout <<"Time " << ms << std::endl;
    //  std::cout << "Width " << width << std::endl;
    sprite.setTextureRect(sf::IntRect(0, 0, width ,64));
}
void FireCone::draw(sf::RenderWindow* window)
{
    if(visible)
    {
        window->draw(Entity::sprite);
        //  window->draw(Entity::hitbox);
        //std::cout << "drawing cone sprit" << std::endl; 
    }
}




