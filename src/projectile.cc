#include <SFML/Graphics.hpp>
#include <math.h>
#include <iomanip>
#include "worm.h"
#include "projectile.h"
#include "map_class.h"
#include "audio_lib.h"

#define PI 3.14159265359



using namespace sf;

float rad(float deg) // Omvandla grader till radianer 
{
    return deg*(PI/180);
}

Projectile::Projectile(Vector2f size,Vector2f pos,float rotation,float speed,std::string texture,
                       std::vector<Entity*>* objects_ptr, float kg, float damage)
    :Entity(size,pos,texture),
     objectsArrayPtr{objects_ptr}, 
     velocity{(float)cos(rad(rotation)) * speed,(float)sin(rad(rotation)) * speed},
    maxdamage{damage},blast_radius{(size.x * size.y/2)}, weight{kg}
{
    sprite.setScale(-1,1);
    sprite.setOrigin(sprite.getGlobalBounds().width/2,sprite.getGlobalBounds().height/2);
    hitbox.setScale(-1,1);
    hitbox.setOrigin(hitbox.getGlobalBounds().width/2,hitbox.getGlobalBounds().height/2);
    hitbox.setRotation(rotation);
    sprite.setRotation(rotation);
}

void Projectile::updateMovement()
{
   
}

void Projectile::update(View& view, Map_class & map)
{
    velocity.y += 0.001 * weight;
    if(sprite.getRotation() > 90) sprite.setRotation(sprite.getRotation() - (weight * 0.1));
    if(sprite.getRotation() < 90) sprite.setRotation(sprite.getRotation() + (weight * 0.1));
    hitbox.move(velocity);
    sprite.setPosition(hitbox.getPosition());
    view.setCenter(hitbox.getPosition());

    // Kollar att objekt �r innanf�r ramen f�r Image
    if (!map.insideImageFrame(hitbox.getGlobalBounds()))
    {
        hit = true;
        return;
    }
    // Kollar kollision med karta samt alla Worm
    else if(collision(map) || collisionWorm())
    {
        sf::CircleShape circle{blast_radius};
        circle.setPosition(hitbox.getPosition().x  - circle.getRadius(),
                           hitbox.getPosition().y  - circle.getRadius());
        map.eraseCircle(circle);
        hit = true;
        makeExplosion(circle);
    }
    
}

void Projectile::draw(sf::RenderWindow * window)
{
		window->draw(Entity::sprite);
		//window->draw(Entity::hitbox);
}

sf::Vector2f normalize(sf::Vector2f& source)
{
    float length = sqrt((source.x * source.x) + (source.y * source.y));
    if (length != 0)
        return sf::Vector2f(source.x / length, source.y / length);
    else
        return source;
}
               
sf::Vector2f calc_power_vect(sf::FloatRect const & worm, sf::CircleShape const & expl)
{
    std::cout << "Explosion center \tx: "<< (expl.getPosition().x + expl.getRadius()) << "\ty: " << (expl.getPosition().y + expl.getRadius()) << std::endl;
    std::cout  << "Worm center \t\tx: " <<  worm.left + worm.width/2 << "\ty: " << worm.top + worm.height/2 << std::endl;

    int diff_x =  (worm.left + worm.width/2) - (expl.getPosition().x + expl.getRadius());
    int diff_y =  (worm.top + worm.height/4) - (expl.getPosition().y + expl.getRadius());
         
    if (hypot(diff_x,diff_y) > expl.getRadius(  )    )
    {
        std::cout << "Difference bigger than radius \n";
        return sf::Vector2f(0,0);
    }
    std::cout << "Diffrence  \t\tx: "<< diff_x << "\ty :" << diff_y << std::endl;
    sf::Vector2f vektor(diff_x,diff_y);
    return (normalize(vektor) * (float)expl.getRadius() - vektor);
}

void Projectile::makeExplosion(sf::CircleShape const & circle)
{
    sf::CircleShape bigger_circle{circle.getRadius()};
		Audio_lib::playSound("Boom.wav");
		Audio_lib::stopSound("ShootShot.wav");
    bigger_circle.setPosition(circle.getPosition());
    for (Entity* e : *objectsArrayPtr)
    {
        if (e->getGlobalBounds().intersects(circle.getGlobalBounds()))
        {
            sf::Vector2f kraftpil = calc_power_vect(e->getGlobalBounds(), bigger_circle);
            dynamic_cast<Worm*>(e)->setVelocity(kraftpil / circle.getRadius() *(float)(
                                                   fabs(velocity.x + velocity.y) * 1.2f *2.f));
            dynamic_cast<Worm*>(e)->setDamage(fabs(kraftpil.x + kraftpil.y)/2);//maxdamage );//* hypot(kraftpil.x, kraftpil.y)/(bigger_circle.getRadius() - Entity::getGlobalBounds().width));
            std::cout  << "Applicerad kraftpil \tx: " << kraftpil.x << "\ty: " << kraftpil.y << std::endl;
        }
    }
}

bool Projectile::collision(Map_class & map)
{
    return map.collision(hitbox.getGlobalBounds());
}

bool Projectile::collisionWorm()
{
    for(Entity* const& e : *objectsArrayPtr)
    {
        if (hitbox.getGlobalBounds().intersects(e->getGlobalBounds()))
            return true;
    }
    return false;
}
