
#ifndef OPTIONS_H
#define OPTIONS_H
#include <SFML/Graphics.hpp>
#include "state.h"
#include <SFML/Audio.hpp>
class Options : State
{
public:
    Options();
    void draw(sf::RenderWindow* window);
    void update(sf::RenderWindow* window);
    int getplayer();
    int getworm();
    std::string getMap();
private:
    sf::Texture texturebackground;
    sf::Texture maptexture;
    sf::Sprite background;
    sf::Texture texturearrow;
    sf::Texture texturepressedarrow;
    sf::Texture wormtexture;
    sf::Sprite backarrow;
    sf::Sprite addwormarrow;
    sf::Sprite subwormarrow;
    sf::Sprite addplayerarrow;
    sf::Sprite subplayerarrow;
    sf::Sprite wormsprite;
    sf::Sprite nextmap;
    sf::Sprite prevmap;
    sf::Sprite mapsprite;

    sf::Text headline;
    sf::Text worms;
    sf::Text player;
    sf::Text playertext;
    sf::Text wormtext;
    sf::Text maptext;

    float wormrotate;

    int wormcount;
    int playercount;
    float xmove;
    float ymove;

    sf::Clock clock;
    std::vector<sf::Sprite> arrows;

    std::vector<bool> boop;

    std::vector<std::string> maps;
    std::vector<std::string>::iterator currentmap;


    void bounce(sf::Sprite & s);
    void setnext();
    void setprev();
    void setmap();
};

#endif
