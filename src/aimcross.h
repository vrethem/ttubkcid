#ifndef AIMCROSS_H
#define AIMCROSS_H
#include"entity.h"


class Aim: public Entity 
{
public:
    Aim()=delete;
    Aim(sf::Vector2f const & pos);
    void setPosition(sf::Vector2f const & pos);
    void setOrigin(sf::Vector2f const & pos);
    sf::Vector2f getPosition() const
    {
        return Entity::hitbox.getPosition();
    }
    void draw(sf::RenderWindow* window) override;
    void update(sf::View &, Map_class &) override;  
};



#endif
