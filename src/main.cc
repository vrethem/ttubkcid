#include <SFML/Graphics.hpp>
#include "game_state.h"

int main()
{

    sf::RenderWindow window(sf::VideoMode(1400, 800), "ttubkcid");
    Game_State game{window};

    sf::Clock gameClock{};
    while (window.isOpen())
    {
       window.setFramerateLimit(80);
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
        
        // if(gameClock.getElapsedTime().asMicroseconds() >= 166)
        {
            game.update();
            window.clear(sf::Color::White);
            game.draw();
            gameClock.restart();
        }
        window.display();
    }

    return 0;
}
