
#ifndef WORLD_H
#define WORLD_H
#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>
#include <SFML/Audio.hpp>
#include "state.h"
#include "map_class.h"
#include "entity.h"
#include "player.h"
class World : State
{
public:
    World(sf::RenderWindow* _window);
    World(World const &) = delete;
    World operator=(World const &) = delete;
    void draw();
    void update();
    void start(int const _pcount,int const _wcount,std::string const & mapname);
void restart();    
	void clear();
private:
    void next_player();
    void moveWater();
    
    std::vector<std::unique_ptr<Player>> playerv;

    std::vector<Entity*> objects;
    Map_class map;
    sf::View view;
    std::vector<std::unique_ptr<Player>>::iterator currentplayer;
    bool gameover;
    sf::RenderWindow* window;
    int wcount{};
    int pcount{};
    int roundtime{60};
    bool newRound{false};

    sf::Text time;
    sf::Sprite clock;
    sf::Sprite water;
    sf::Sprite background;

    sf::Clock timer;
    sf::Text playertext{};

		// For debug
		int counter{}; 
};


#endif
