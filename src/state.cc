
#include "state.h"

State::states State::state{state_menu};

void State::changeState(std::string s)
{
    if (s=="menu")
				State::state = state_menu;
    else if (s=="world")
				State::state = state_world;
    else if (s == "score_screen")
				State::state = state_score_screen;
    else if (s == "options")
				State::state = state_options;
    else if (s == "ingame_menu")
				State::state = state_ingame_menu;
    else if (s == "credits")
				State::state = state_credits;
}
std::string State::getState()
{
    if (State::state == state_menu)
	return "menu";
    else if (State::state == state_world)
	return "world";
    else if (State::state == state_score_screen)
	return "score_screen";
    else if (State::state == state_options)
	return "options";
    else if (State::state == state_ingame_menu)
	return "ingame_menu";
    else
	return "credits";
}
