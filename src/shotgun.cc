#include "shotgun.h"


Shotgun::Shotgun(std::vector<Entity*>* obj_ptr )
    :Weapon{"shotgun", shotgun_damage, obj_ptr, sf::Vector2f{32, 20},sf::Vector2f{200,50}}
    {}

void Shotgun::fire(float speed) 
{
    if (!active)
    {
        throw std::logic_error("fiering inactive Weapon");
    }
    worm_state = duringFire;

    projectiles.emplace_back(Projectile{projectile_size,aim_point.getPosition(), Entity::hitbox.getRotation(), speed,name + "_shot.png", objectsPtr, weight, shotgun_damage});
    projectiles.emplace_back(Projectile{projectile_size,aim_point.getPosition(), Entity::hitbox.getRotation() + 12.f, speed,name + "_shot.png", objectsPtr, weight, shotgun_damage});
    projectiles.emplace_back(Projectile{projectile_size,aim_point.getPosition(), Entity::hitbox.getRotation() - 12.f, speed,name + "_shot.png", objectsPtr, weight, shotgun_damage});
    std::cout<< "Shotgun!!!" << std::endl;
}


void Shotgun::prepareFire()
{
     std::cout<< "Prepare the Shotgun!!!" << std::endl;
    fire_time.restart();
    worm_state = Fire;
}

void Shotgun::fireUpdate() 
{
    if(worm_state == Fire)
    {
        fire(def_speed);
        worm_state = duringFire;
    }
}
