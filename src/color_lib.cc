#include "color_lib.h"

std::vector<sf::Color> Color_lib::colors{sf::Color::Red,sf::Color::Blue,sf::Color::Green,sf::Color::Magenta};
    
sf::Color Color_lib::getColor()
{
    sf::Color color = colors.front();
    colors.erase(colors.begin());
    return color;
}
void Color_lib::restart()
{
    colors = std::vector<sf::Color>{sf::Color::Red,sf::Color::Blue,sf::Color::Green,sf::Color::Magenta};
}
