

#ifndef CREDITSWORM_H
#define CREDITSWORM_H
#include <SFML/Graphics.hpp>
#include <string>
#include <SFML/Audio.hpp>
class Creditsworm
{
public:
    Creditsworm(std::string _name,sf::Color const & color);
    void right();
    void left();
    void jump();
    void move();
    void draw(sf::RenderWindow* window);
    sf::Vector2f getPos();
    void reset();
    void rare();
    void setPos(sf::Vector2f const & pos);
private:
    void animate();
    sf::Sprite sprite;
    sf::Text name;
    sf::Clock animation_time;
    sf::Clock rare_time;
    bool order;
    sf::Vector2f velocity{ 0,0 };
    bool jumping;
    bool rareanimation;

    sf::Sound compress;
    sf::Sound expand;
};
#endif
