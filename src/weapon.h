#ifndef WEAPON_H
#define WEAPON_H

#include<string>
#include<vector>
#include"projectile.h"
#include"entity.h"
#include<SFML/System/Vector2.hpp>
#include<SFML/System/Time.hpp>
#include <memory>
#include "aimcross.h"
#include "firecone.h"

#define BAZOKA_DAMAGE 50
#define BAZOOKA_SIZE_X 30
#define BAZOOKA_SIZE_Y 20

enum wormState  { beforeFire, Fire, duringFire, afterFire };

class Weapon : public Entity
{

public:
    Weapon()=delete;
    Weapon(Weapon&&)=default;
    Weapon(const Weapon&)=default; 
    Weapon(std::string nam,float dam,
           std::vector<Entity*>* const & obj_ptr, sf::Vector2f const & size,
           sf::Vector2f const & pos);
    ~Weapon()= default;
    Weapon& operator=(const Weapon & rhs)=default;
    
    // comapar operatort // 
    bool operator<(Weapon const& right) const {return name<right.name;}
    bool operator>(Weapon const& right) const {return right<*this;}
    bool operator==(Weapon const& right) const {return (*this<right) && (right<*this);}
    bool operator!=(Weapon const& right) const {return !(*this==right);}
    bool operator>=(Weapon const& right) const {return !(*this<right);}
    bool operator<=(Weapon const& right) const {return !(*this>right);}
    // Setter
    void setVisible()	{visible=true;}
    void setInvisible()	{visible=false;}
    void setActive()	{active=true;}
    void setInactive()	{active=false;}
    void setElevation(float rot)	{Entity::hitbox.setRotation(rot);}
    // Getter
    int getRotation();
    sf::Vector2f getPosition();
    std::string getName() const {return name;}

    void incElevation();
    void decElevation();
    void setPosition(sf::Vector2f const & pos);
    void moveLeft();
    void moveRight();
    void draw(sf::RenderWindow*) override;
    void update(sf::View &, Map_class &) override;
    bool isProjectilesEmpty() {return projectiles.empty();}
    void resetWeapon();
    void skipFire();
    wormState getState();
    virtual  void prepareFire()=0;
    virtual void fireUpdate()=0;
    bool isLeft() { return left; }
    void copyRotation(Weapon* const & w)
    {
        left = w->isLeft();
        Entity::hitbox.setRotation(w->getRotation());
        worm_state = w->getState();
    }

protected:
    std::string name;
    float damage;
    Aim aim_point;
    std::vector<Entity*>* objectsPtr;		// Gör om till iterator // även denna ???
    std::vector<Projectile> projectiles;	
    bool active=false;
    bool visible=false;
    bool left=true;

    sf::Clock fire_time{};

    wormState worm_state{beforeFire};
    FireCone cone;

    virtual void fire(float f) = 0;
};

#endif

