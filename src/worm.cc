#include"worm.h"
#include <exception>
#include "font_lib.h"
#include <iostream>
#include <cstdlib>
#include "audio_lib.h"
#include "name_lib.h"

Worm::Worm(WeaponStash::CircularIterator start_weap, sf::Vector2f const & pos,sf::Color color)
    :Entity{ sf::Vector2f(W_SIZE_X,W_SIZE_Y), pos, "test.png" },
    selected_weapon{start_weap},jump{},doublejump{},rare_time_max{},animation_time{},rare_time{},
    name{"name",Font_lib::getFont("arial.ttf"),12},
    health{"health",Font_lib::getFont("arial.ttf"),12}
{
    sprite.setTextureRect(sf::IntRect(0, 0, 32, 40));
    name.setString(Name_lib::getName());
    name.setFillColor(color);
    health.setFillColor(color);
    rare_time_max = std::rand() % 30 + 10;
}

void Worm::dance()
{
    sf::IntRect spriteRect = sprite.getTextureRect();
    if(animation_time.getElapsedTime().asSeconds() >= 0.4f)
    {
        if(spriteRect.left < 320 || spriteRect.left > 352) spriteRect.left = 320;
        spriteRect.left += 32;
        animation_time.restart();
    }
    sprite.setTextureRect(spriteRect);
}

void Worm::Animate()
{
    sf::IntRect spriteRect = sprite.getTextureRect();
    if(animation_time.getElapsedTime().asSeconds() >= 0.4f)
    {
        if(movement == sf::Vector2f(0,0) || (movement.y > 0 && movement.y < 1))
        {
            if(rare_time.getElapsedTime().asSeconds() >= rare_time_max)
            {
                //Rare worm Idle animation
                if(spriteRect.left < 224) spriteRect.left = 224;
                else if(spriteRect.left >= 288) spriteRect.left = 256; 
                else spriteRect.left += 32;
                animation_time.restart();
                if(rare_time.getElapsedTime().asSeconds() >= rare_time_max+5)
                {
                    spriteRect.left = 224;
                    rare_time.restart();
                }
            }
            else
            {
                //worm Idle animation
                if(spriteRect.left >= 32) spriteRect.left = 0;
                else spriteRect.left += 32;
                animation_time.restart();
            }
        }
    }
    if(animation_time.getElapsedTime().asSeconds() >= 0.1f)
    {
        if(jump.getElapsedTime().asSeconds() < 0.5)
        {
            //worm Jumping animation
            if(spriteRect.left < 128 || spriteRect.left > 192) spriteRect.left = 128;
            else if(spriteRect.left < 192) spriteRect.left += 32;
            animation_time.restart();	
        }
        if(doublejumping && !notjumping)
        {
            if(spriteRect.left < 320)spriteRect.left = 320;
            else if(spriteRect.left == 320)spriteRect.left = 416;
            else if(spriteRect.left == 416)spriteRect.left += 32;
            else if(spriteRect.left == 448)spriteRect.left = 320;
            animation_time.restart();
        }	
    }
    if(animation_time.getElapsedTime().asSeconds() >= 0.2f)
    {
        if(movement.x != 0)
        {
            //Moving animation for worm
            if(movement.y < 1 && movement.y > -1)
            {
                //32 -> 64 -> 96 -> 64 -> 32
                if(spriteRect.left < 32 || spriteRect.left > 96)
                    spriteRect.left = 32;
                else if(spriteRect.left == 32)		
                {
                    Audio_lib::playSound("Walk-Compress.wav");
                    spriteRect.left += 32;
                    order = true;
                }
                else if(spriteRect.left == 64)
                {
		    
                    if(order)spriteRect.left += 32;
                    else spriteRect.left -= 32;
                }
                else if(spriteRect.left == 96)
                {
                    Audio_lib::playSound("Walk-Expand.wav");
                    spriteRect.left -= 32;
                    order = false;
                }
            }
            animation_time.restart();
        }
    }
    if(movement.y > 0.5)spriteRect.left = 320;
    sprite.setTextureRect(spriteRect);
}

void Worm::draw(sf::RenderWindow* win_ptr)
{
    win_ptr->draw(name);
    win_ptr->draw(health);
    win_ptr->draw(Entity::sprite);
    selected_weapon->first->draw(win_ptr); // H�r k�rs draw funktion p� wormens vapen, vilket g�r att vi inte beh�ver pekare till vapen i World klassen
    //win_ptr->draw(Entity::hitbox);
}

bool Worm::collision(Map_class & map)
{
    if (!map.insideImageFrame(hitbox.getGlobalBounds()) )
    { // Delete worm -- Outside map
        Audio_lib::playSound("Splash.wav");
        hp = 0;
        selected_weapon->first->setInvisible();
        selected_weapon->first->skipFire();
        return false;
    }
    return map.collision(hitbox.getGlobalBounds());
}
void Worm::setVelocity(sf::Vector2f const & velo)
{
    velocity = velo;
    movement += velocity;
}

void Worm::update(sf::View & view, Map_class & map)
{
    hitbox.move(movement);
    sprite.setPosition(hitbox.getPosition() - sf::Vector2f(5, 0));
    health.setString(std::to_string(hp));
    name.setPosition(hitbox.getPosition().x, hitbox.getPosition().y - hitbox.getGlobalBounds().height / 2 - health.getGlobalBounds().height);
    health.setPosition(hitbox.getPosition().x + hitbox.getGlobalBounds().width / 2, hitbox.getPosition().y - hitbox.getGlobalBounds().height / 2);

    if (active)
        updateActive(view, map);
   
    updateGravity(map);
    if (movement.x < 0.1 && movement.x > -0.1) movement.x = 0;
    //  if (velocity.y < 0.1 && velocity.y > -0.1) velocity.y = 0;
    Animate();
}

void Worm::updateActive(sf::View & view, Map_class & map)
{
    selected_weapon->first->setPosition(hitbox.getPosition() + hitbox.getSize());
    selected_weapon->first->update(view, map);

    if (collision(map))
    {
        float heightcheck = 1.2;

        hitbox.move(movement.x, -heightcheck);
        if (collision(map))
        {
            hitbox.move(-movement.x, heightcheck);
        }

        hitbox.move(-movement);
        if (movement.y > 0)movement.y = 0;
        notjumping = true;
    }
    else
    {
        if (jump.getElapsedTime().asSeconds() < 0.3 && !notjumping && !doublejumping)
        {
            if (movement.x < maxspeed && movement.x > -maxspeed)
                movement.x += (sprite.getScale().x * -1 * wormSpeed);
            movement.y -= jumpSpeed;
        }
        else if (jump.getElapsedTime().asSeconds() < 0.4 && !notjumping && doublejumping)
        {
            if (movement.x < maxspeed && movement.x > -maxspeed)
                movement.x += (sprite.getScale().x * wormSpeed/4);
            movement.y -= jumpSpeed;
        }
    }

    
    if (notjumping)
    {
        if (movement.x < 0)movement.x += wormSpeed / 10;
        if (movement.x > 0)movement.x -= wormSpeed / 10;
    }
     
     if (selected_weapon->first->getState() == duringFire && (selected_weapon->first->isProjectilesEmpty()) )
    {
        active = false;
        selected_weapon->first->setInvisible();
        selected_weapon->first->setInactive();
    }
        
    view.setCenter(hitbox.getPosition()); //Camera?  
}

void Worm::updateGravity(Map_class & map)
{
    if (movement != sf::Vector2f(0, 0))
    {
        if (collision(map))
        {
            notjumping = true;
            hitbox.move(-movement);
            movement.y *= 0.3;
            movement.x *= 0.9;
        }
        else
        {
            movement.y += gravity;
        }
    }
}

void Worm::updateMovement()
{
    if (!active) throw std::logic_error("<Worm> Trying to move inactive worm: Worm::active == false\n");

    if(movement.y < 0.2 && movement.y > -0.2 && notjumping) selected_weapon->first->setVisible();
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && notjumping && (selected_weapon->first->getState() != Fire))
    {
        selected_weapon->first->setInvisible();
        selected_weapon->first->moveRight();

        sprite.setOrigin(32, 0);
        sprite.setScale(abs(sprite.getScale().x)*-1, sprite.getScale().y);

        if (movement.x < maxspeed)
            movement.x += wormSpeed;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
    {
        selected_weapon->first->incElevation();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
    {
        selected_weapon->first->decElevation();
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) && notjumping && (selected_weapon->first->getState() != Fire))
    {

        selected_weapon->first->moveLeft();
        selected_weapon->first->setInvisible();
        sprite.setOrigin(0, 0);
        sprite.setScale(abs(sprite.getScale().x), sprite.getScale().y);

        if (movement.x > -maxspeed)
            movement.x -= wormSpeed;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && (selected_weapon->first->getState() != Fire) && jump.getElapsedTime().asSeconds() > 0.3)
    {
        selected_weapon->first->setInvisible();
        if (notjumping) 
        { 
            doublejumping = false;
            Audio_lib::playSound("jump.wav");
            doublejump.restart();
            jump.restart();
            notjumping = false;
        }
        if(doublejump.getElapsedTime().asSeconds() < 0.4 && doublejump.getElapsedTime().asSeconds() > 0.3 && !doublejumping)
        {
            movement.x *= -0.5;
            doublejumping = true;
        }
    }
    //change weapon
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::C) )
    {
        change = true;
    }
    else 
    { 
        if(change) 
        {
            Weapon* old_rot = selected_weapon->first;
            inactivateWeapon();
            selected_weapon->first->setInvisible();
            selected_weapon->first->resetWeapon();
            
            selected_weapon++; 
        
            activateWeapon();
            selected_weapon->first->setVisible();
            selected_weapon->first->copyRotation(old_rot);
        }
        change = false;
    }
    
    // fire button
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) )
    {
        if(selected_weapon->first->getState() == beforeFire && movement.y == 0)
        {
            selected_weapon->first->setVisible();
            selected_weapon->first->prepareFire();
            
            Audio_lib::playSound("LoadShot.wav");
        }
    }
    selected_weapon->first->fireUpdate();  
}

int Worm::getHealth()
{
    return hp;
}

void Worm::setActive()
{
    active = true;
    selected_weapon->first->resetWeapon();
}

void Worm::setInactive()
{
    active = false;
    inactivateWeapon();
}
bool Worm::getActive()
{
    return active;
}
void Worm::activateWeapon()
{
    selected_weapon->first->setActive();
}
void Worm::inactivateWeapon()
{
    selected_weapon->first->setInactive();
}

//Private



