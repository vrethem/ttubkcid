#ifndef AUDIO_LIB_H
#define AUDIO_LIB_H
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <map>
class Audio_lib
{
public:
    static sf::SoundBuffer& getSound(std::string const & s);
    static void playSound(std::string const & s);
    static void stopSound(std::string const & s);
private:
    static std::map<std::string,sf::Sound> slib;
    static std::map<std::string,sf::SoundBuffer> lib;
};
#endif
