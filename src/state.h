#ifndef STATE_H
#define STATE_H
#include <string>
class State
{
public:
    State() = default;
    std::string getState();
protected:
    void changeState(std::string s);
private:
    enum states{state_menu,state_world,state_options,state_score_screen,state_ingame_menu,state_credits};
    static states state;
};
#endif
