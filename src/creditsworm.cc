#include "creditsworm.h"
#include "texture_lib.h"
#include "font_lib.h"
#include <string>
#include "audio_lib.h"

Creditsworm::Creditsworm(std::string _name,sf::Color const & color) : sprite{Texture_lib::getTexture("test.png")},name{},animation_time{},rare_time{},order{false},jumping{false},rareanimation{false},compress{},expand{}
{
    //compress.setBuffer(Audio_lib::getSound("Walk-Compress.wav"));
    //expand.setBuffer(Audio_lib::getSound("Walk-Expand.wav"));

    sprite.setTextureRect(sf::IntRect(0,0,32,40));
    sprite.setScale(5.0f,5.0f);
    sprite.setPosition(850,600);
    name.setFont(Font_lib::getFont("arial.ttf"));
    name.setPosition(800,550);
    name.setString(_name);
    name.setFillColor(color);
    name.setCharacterSize(55);
    name.setOutlineColor(sf::Color::Black);
    // name.setOutlineThickness(1.0f);
    name.setStyle(sf::Text::Bold || sf::Text::Italic);
}
void Creditsworm::right()
{
    if (!rareanimation)
    {
	velocity.x = 0.5;
	sprite.setOrigin(32,0);
	name.setOrigin(-32,0);
	sprite.setScale(abs(sprite.getScale().x)*-1,sprite.getScale().y);
    }
}
void Creditsworm::left()
{
    if (!rareanimation)
    {
	velocity.x = -0.5;
	name.setOrigin(0,0);
	sprite.setOrigin(0,0);
	sprite.setScale(abs(sprite.getScale().x),sprite.getScale().y);
    }
}
void Creditsworm::jump()
{
    if (!jumping && !rareanimation)
    {
	velocity.y = -1.5;
	jumping = true;
    }
}
void Creditsworm::setPos(sf::Vector2f const & pos)
{
    sprite.setPosition(pos);
    name.setPosition(sf::Vector2f{pos.x-50,pos.y-50});
}
void Creditsworm::move()
{
    sprite.move(velocity);
    name.move(velocity);
    if (jumping || sprite.getPosition().y < 600)
    {
	velocity.y += 0.01;
	if (sprite.getPosition().y >= 600)
	{
	    velocity.y = 0;
	    jumping = false;
	}
    }
    animate();
}
void Creditsworm::rare()
{
    rare_time.restart();
    rareanimation = true;
    velocity = sf::Vector2f{0,0};
}
void Creditsworm::animate()
{
    sf::IntRect spriteRect = sprite.getTextureRect();
    if(animation_time.getElapsedTime().asSeconds() >= 0.5f)
    {
	if(velocity == sf::Vector2f(0,0))
	{
	    if(rareanimation)
	    {
		//Rare worm Idle animation
		if(spriteRect.left < 224) spriteRect.left = 224;
		else if(spriteRect.left >= 288) spriteRect.left = 256; 
		else spriteRect.left += 32;
		animation_time.restart();
		if(rare_time.getElapsedTime().asSeconds() >= 5)
		{
		    spriteRect.left = 224;
		    rareanimation = false;
		}
	    }
	    else
	    {
		//worm Idle animation
		if(spriteRect.left >= 32) spriteRect.left = 0;
		else spriteRect.left += 32;
		animation_time.restart();
	    }
	}
    }
    if(animation_time.getElapsedTime().asSeconds() >= 0.1f)
    {
	if(velocity.y != 0)
	{
	    //worm Jumping animation
	    if(spriteRect.left < 128 || spriteRect.left > 192) spriteRect.left = 128;
	    else if(spriteRect.left < 192) spriteRect.left += 32;
	    animation_time.restart();		
	}
    }
    if(animation_time.getElapsedTime().asSeconds() >= 0.4f)
    {
	if(velocity != sf::Vector2f(0,0))
	{
	    //Moving animation for worm
	    if(velocity.y == 0)
	    {
		//32 -> 64 -> 96 -> 64 -> 32
		if(spriteRect.left < 32 || spriteRect.left > 96)
		    spriteRect.left = 32;
		else if(spriteRect.left == 32)
		{
            Audio_lib::playSound("Walk-Expand.wav");
		    spriteRect.left += 32;
		    order = true;
		}
		else if(spriteRect.left == 64)
		{
		    if(order)spriteRect.left += 32;
		    else spriteRect.left -= 32;
		}
		else if(spriteRect.left == 96)
		{
            Audio_lib::playSound("Walk-Compress.wav");
		    spriteRect.left -= 32;
		    order = false;
		}
	    }
	    animation_time.restart();
	}
    }
    sprite.setTextureRect(spriteRect);

}
void Creditsworm::draw(sf::RenderWindow* window)
{
    window->draw(sprite);
    window->draw(name);
}
sf::Vector2f Creditsworm::getPos()
{
    return sprite.getPosition();
}
void Creditsworm::reset()
{
    sprite.setTextureRect(sf::IntRect(0,0,32,40));
    sprite.setPosition(850,600);
    name.setPosition(800,550);

    velocity = sf::Vector2f(0.0f,0.0f);
    jumping = false;
    rareanimation = false;
    
}
