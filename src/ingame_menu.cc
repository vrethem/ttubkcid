#include "ingame_menu.h"
#include <thread>
#include "font_lib.h"
#include "audio_lib.h"
Ingame_menu::Ingame_menu(World* _world) : board{sf::Vector2f(400,400)},resume{},restart{},exit{},boop{},world{_world}
{
    board.setPosition(400,200);
    board.setFillColor(sf::Color::Black);
    // board.setOutlineColor(sf::Color::Blue);
    // board.setOutlineThickness(4.0f);

    //boop.setBuffer(Audio_lib::getSound("buttonhover.wav"));

    resume.setFont(Font_lib::getFont("arial.ttf"));
    restart.setFont(Font_lib::getFont("arial.ttf"));
    exit.setFont(Font_lib::getFont("arial.ttf"));
    
    resume.setPosition(board.getPosition().x + 100,board.getPosition().y + 50);
    restart.setPosition(board.getPosition().x + 100,board.getPosition().y + 150);
    exit.setPosition(board.getPosition().x + 100,board.getPosition().y + 250);

    resume.setString("Resume");
    restart.setString("Restart");
    exit.setString("Exit");

    resume.setCharacterSize(60);
    restart.setCharacterSize(60);
    exit.setCharacterSize(60);

    resume.setFillColor(sf::Color::Red);
    restart.setFillColor(sf::Color::Red);
    exit.setFillColor(sf::Color::Red);

    /*resume.setOutlineColor(sf::Color::Black);
    restart.setOutlineColor(sf::Color::Black);
    exit.setOutlineColor(sf::Color::Black);

    resume.setOutlineThickness(1.0f);
    restart.setOutlineThickness(1.0f);
    exit.setOutlineThickness(1.0f);   */
}
void Ingame_menu::draw(sf::RenderWindow* window)
{
    window->draw(board);
    window->draw(resume);
    window->draw(restart);
    window->draw(exit);
}
void Ingame_menu::update(sf::RenderWindow* window)
{
    sf::Vector2i mousepos = sf::Mouse::getPosition(*window);

    if (mousepos.x >= resume.getPosition().x && mousepos.x <= resume.getPosition().x + 200 && mousepos.y >= resume.getPosition().y && mousepos.y <= resume.getPosition().y + 100)
    {
	if (resume.getFillColor() != sf::Color::White)
	{
        Audio_lib::playSound("buttonhover.wav");
	    resume.setFillColor(sf::Color::White);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
	    changeState("world");
	}
    }
    else
    {
	resume.setFillColor(sf::Color::Red);
    }
    if (mousepos.x >= restart.getPosition().x && mousepos.x <= restart.getPosition().x + 200 && mousepos.y >= restart.getPosition().y && mousepos.y <= restart.getPosition().y + 50)
    {
	if (restart.getFillColor() != sf::Color::White)
	{
        Audio_lib::playSound("buttonhover.wav");
	    restart.setFillColor(sf::Color::White);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
	    world->clear();
	    std::this_thread::sleep_for (std::chrono::milliseconds(300));
	    changeState("menu");
	}
    }
    else
    {
	restart.setFillColor(sf::Color::Red);
    }
    if (mousepos.x >= exit.getPosition().x && mousepos.x <= exit.getPosition().x + 100 && mousepos.y >= exit.getPosition().y && mousepos.y <= exit.getPosition().y + 50)
    {
	if (exit.getFillColor() != sf::Color::White)
	{
        Audio_lib::playSound("buttonhover.wav");
	    exit.setFillColor(sf::Color::White);
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{

	    window->close();
	}
    }
    else
    {
	exit.setFillColor(sf::Color::Red);
    }
}
