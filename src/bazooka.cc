#include "bazooka.h"
#include<SFML/System/Vector2.hpp>

Bazooka::Bazooka(std::vector<Entity*>* obj_ptr)
    :Weapon{"bazooka",BAZOKA_DAMAGE,obj_ptr,sf::Vector2f{BAZOOKA_SIZE_X,BAZOOKA_SIZE_Y},sf::Vector2f{200,50}}
{}


void Bazooka::prepareFire()
{
    fire_time.restart();
    cone.setVisible();
    worm_state = Fire;
    std::cout << "Prepare BAZOOKA\n";
}

void Bazooka::fireUpdate()
{
    if(worm_state == Fire)
    {
        cone.setRotation( Entity::hitbox.getRotation() );
        cone.setPosition( Entity::hitbox.getPosition() );
       
        cone.updateTexture((int)fire_time.getElapsedTime().asMilliseconds());  
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
    {
        if (worm_state == Fire && ( fire_time.getElapsedTime().asSeconds() > 2 ))
            fire(fire_time.getElapsedTime().asSeconds());
    }
    else 
    {
        if (worm_state == Fire)
        {
            fire(fire_time.getElapsedTime().asSeconds());  
        }
    }
}
void Bazooka::fire(float speed)
{
        std::cout << "FIRE BAZOOKA\n";
    if (!active)
    {
        throw std::logic_error("fiering inactive Weapon");
    }
    worm_state = duringFire;
    if (speed < 0.6) speed = 0.6f;
    if (speed > 2) speed = 2;
    Audio_lib::stopSound("LoadShot.wav");
    Audio_lib::playSound("ShootShot.wav");
    projectiles.emplace_back(Projectile{projectile_size,aim_point.getPosition(), Entity::hitbox.getRotation(), speed*1.5f,"bazooka_shot.png", objectsPtr, weight, damage});

    
    std::cout<< "fiering weapon from X,Y:" << (aim_point.getPosition()).x << "," << (cone.getPosition()).y << "  with speed: " << speed  << std::endl;
    cone.setInvisible();
    
    
}
