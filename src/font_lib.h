#ifndef FONT_LIB_H
#define FONT_LIB_H

#include <SFML/Graphics.hpp>
#include <string>
#include <map>
class Font_lib
{
public:
    static sf::Font& getFont(std::string s);
    //static void addFont(std::string s);
private:
    static std::map<std::string,sf::Font> lib;
};


#endif

