#ifndef WORM_H
#define WORM_H
#include "weaponstash.h"
#include "entity.h"
#include "map_class.h"
#include "weapon.h"
#include <utility>
#include <map>
#include <SFML/Audio.hpp>
#define WORM_HEALTH 100

typedef std::pair<int, int> Point;
class Worm:  public Entity
{
public:
    Worm(WeaponStash::CircularIterator start_weap, sf::Vector2f const & pos,sf::Color color);
    virtual ~Worm() { }
    Worm(sf::Vector2f const & pos);
   // Worm(sf::Vector2f const & pos) = delete;

    void dance();
    void draw(sf::RenderWindow*) override;
    void updateMovement(); // Map<Weapon, int>* weapon_container);
    void update(sf::View &, Map_class &) override;

    bool collision(Map_class &) override;
    
    //Setters
    void setActive();
    void setInactive();
    void activateWeapon();
    void inactivateWeapon();
    void setVelocity(sf::Vector2f const & velo);
    void setDamage(int dmg) { damage += dmg; }
    void setHealth(int health) { hp = health; }
    
    //Getters
    int getHealth();
    bool getActive();
    int getDamage() { int send{damage}; damage = 0; return send; }
   
    //Debug functions
    Entity* getAdress() override { return this; }
    void printPosition() override
    {
        std::cout << "pos : "<< hitbox.getPosition().x << ", " << hitbox.getPosition().y << std::endl;
    }

private:
    int hp{WORM_HEALTH};
    int damage{};
    float wormSpeed{0.3};//const
    WeaponStash::CircularIterator selected_weapon;

    bool active{false};
    sf::Clock jump;
    sf::Clock doublejump;
    bool doublejumping{false};
    bool notjumping{true};
    
    sf::Vector2f velocity{ 0.0f,0.0f };
    sf::Vector2f movement{ 0.0f, 0.1f };
    int rare_time_max;
    sf::Clock animation_time; 
    sf::Clock rare_time;

    bool order{true};
    bool change{false};
    void Animate();
    void updateActive(sf::View &, Map_class &);
    void updateGravity( Map_class &);
    
    sf::Text name;
    sf::Text health;
    
    float gravity{ 0.05f };
    float jumpSpeed{ 0.12f };
    float maxspeed{0.7};//const
};


#endif
