#ifndef MAP_CLASS_H
#define MAP_CLASS_H

#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <iterator>

#define SPAWN_CROSS 20

typedef std::pair<int, int> point;

class Map_class
{
public:
    //Map_class() = delete;
    Map_class();
    Map_class(std::string map);
    Map_class& operator=(Map_class const & _map);
    Map_class& operator=(Map_class const && _map);

    bool insideImageFrame(sf::FloatRect const &) const;
    bool insideImage(sf::FloatRect const &) const;
    void eraseCircle(sf::CircleShape const &);

    bool collision(sf::FloatRect const &);
    bool collision(sf::CircleShape const &) const;
    
    void draw(sf::RenderWindow*);
    const sf::Image* get_image() const; // Read only ptr
    const std::vector<sf::Vector2f>* get_spawn_points() const; //Read only ptr
    sf::Vector2f get_spawn_point();
    void lowerImage(int y);
    void start(std::string const & name);

private:
    void init();
    void set_sprite();

    std::vector<sf::Vector2f> spawn_points;
    std::vector<sf::Vector2f>::iterator sp_iter;
    sf::Image image;
    sf::Sprite sprite;
    sf::Texture text;
};


#endif
