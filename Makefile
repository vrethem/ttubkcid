#
# Makefile 
#

CCC      = g++
FLAGS += -std=c++17 -pedantic -Wall -Wextra -Weffc++
SFML_FLAGS += -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio
INCLUDE_SFML += -I${SFML_ROOT}/include -L${SFML_ROOT}/lib

OBJECTS = menu.o game_state.o state.o texture_lib.o map_class.o world.o options.o team.o worm.o player.o ingame_menu.o projectile.o credits.o creditsworm.o bazooka.o aimcross.o firecone.o weapon.o weaponstash.o font_lib.o audio_lib.o name_lib.o color_lib.o shotgun.o

main: directories ${OBJECTS} Makefile src/main.cc
	${CCC} ${FLAGS} src/main.cc ${OBJECTS} -o bin/game ${SFML_FLAGS} ${INCLUDE_SFML} 


menu.o: src/menu.h src/menu.cc state.o
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/menu.cc 
game_state.o: src/game_state.h src/game_state.cc menu.o world.o state.o options.o
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/game_state.cc
texture_lib.o: src/texture_lib.h src/texture_lib.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/texture_lib.cc
map_class.o: src/map_class.h src/map_class.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/map_class.cc
state.o: src/state.h src/state.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/state.cc
world.o: src/world.h src/world.cc state.o texture_lib.o font_lib.o
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/world.cc
options.o: src/options.h src/options.cc state.o
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/options.cc
team.o: src/team.h src/team.cc worm.o
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/team.cc
worm.o: src/worm.cc src/worm.h src/entity.h font_lib.o
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/worm.cc
player.o: src/player.h src/player.cc src/team.h 
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/player.cc
ingame_menu.o: src/ingame_menu.h src/ingame_menu.cc src/state.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/ingame_menu.cc
aimcross.o: src/aimcross.h src/aimcross.cc src/entity.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/aimcross.cc
firecone.o: src/firecone.h src/firecone.cc src/entity.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/firecone.cc
weapon.o: src/weapon.cc src/weapon.h src/entity.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/weapon.cc
weaponstash.o: src/weaponstash.cc src/weaponstash.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/weaponstash.cc
bazooka.o: src/bazooka.h src/bazooka.cc src/weapon.h src/entity.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/bazooka.cc
credits.o: src/credits.h src/credits.cc src/state.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/credits.cc
creditsworm.o: src/creditsworm.h src/creditsworm.cc src/credits.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/creditsworm.cc
projectile.o: src/projectile.h src/projectile.cc src/entity.h src/map_class.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/projectile.cc
font_lib.o: src/font_lib.h src/font_lib.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/font_lib.cc
audio_lib.o: src/audio_lib.h src/audio_lib.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/audio_lib.cc
name_lib.o: src/name_lib.h src/name_lib.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/name_lib.cc
color_lib.o: src/color_lib.h src/color_lib.cc
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/color_lib.cc
shotgun.o: src/shotgun.h src/shotgun.cc src/weapon.h
	${CCC} ${FLAGS} ${INCLUDE_SFML} -c src/shotgun.cc
directories: 
	@ \mkdir -p bin


clean:
	@ \rm -f *.o *.gch
	@ \rm -r -f bin
zap:
	@ \rm -f *.~
